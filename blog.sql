-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 12, 2020 at 07:34 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blog`
--

-- --------------------------------------------------------

--
-- Table structure for table `bu`
--

CREATE TABLE `bu` (
  `id` int(11) NOT NULL,
  `bu_name` varchar(100) NOT NULL,
  `bu_place` varchar(50) NOT NULL,
  `image` varchar(300) DEFAULT NULL,
  `rooms` int(11) NOT NULL,
  `bu_price` int(20) NOT NULL,
  `bu_rent` tinyint(1) NOT NULL,
  `bu_square` varchar(10) NOT NULL,
  `bu_type` tinyint(1) NOT NULL,
  `bu_small_dis` varchar(160) DEFAULT NULL,
  `bu_latitude` float DEFAULT NULL,
  `bu_longitude` float DEFAULT NULL,
  `bu_large_dis` longtext NOT NULL,
  `bu_status` tinyint(1) NOT NULL DEFAULT '0',
  `bu_meta` varchar(50) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `month` varchar(3) DEFAULT NULL,
  `year` varchar(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bu`
--

INSERT INTO `bu` (`id`, `bu_name`, `bu_place`, `image`, `rooms`, `bu_price`, `bu_rent`, `bu_square`, `bu_type`, `bu_small_dis`, `bu_latitude`, `bu_longitude`, `bu_large_dis`, `bu_status`, `bu_meta`, `created_at`, `updated_at`, `user_id`, `month`, `year`) VALUES
(1, 'building', '5', '04-large.jpg', 3, 10, 1, 'adghfg', 0, 'Lorem ipsum doloasdasdadad  amnkja ajhfo aoif  asfmpklajf adsjf isdjf isdjkf sdif iuvhs 9is oisj f jssj nos ois hsdh fioushfiou siou phig dgoide iadjf oisajfoij', NULL, -61.38, 'qweqwqweqwqweqwqweqwqweqwqweqwqweqwqweqwqweqwqweqwqweqwqweqwqweqw', 1, NULL, '2018-12-09 08:04:40', '2018-11-27 11:41:23', 1, '08', '2017'),
(2, 'building', '13', '', 5, 50, 1, 'das', 0, 'ddddddddddddddddddddddds', NULL, 6.73, 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quis hic voluptatum optio, temporibus deserunt fugit alias esse similique debitis cupiditate! Dolores nam, rem obcaecati aut laboriosam doloremque earum, deserunt suscipit architecto iste culpa aperiam dolorem! Ipsam at fuga voluptatum nobis quisquam voluptates corrupti nostrum, velit enim, perspiciatis explicabo impedit expedita sit vitae deserunt nemo numquam culpa pariatur omnis dicta odit iste. Soluta adipisci a, unde, suscipit est, natus enim laborum excepturi quas nihil consequuntur magni cupiditate! Enim beatae veritatis delectus inventore eveniet modi dolorem quidem vitae quasi reiciendis reprehenderit voluptatum autem asperiores natus temporibus mollitia doloremque debitis ea aperiam, accusamus animi maiores optio! Veniam ullam dolores accusantium iure non ratione eveniet quam cumque temporibus iusto, commodi iste consequuntur aspernatur neque totam. Id provident, debitis atque ad tempore nulla, delectus nobis voluptate earum consequatur facilis! Doloribus, molestiae. Fuga perspiciatis qui, cum officiis quibusdam ipsum ullam nostrum ratione! Ex consequuntur repellendus minima illo iste id magnam voluptates praesentium doloribus inventore, officiis quod optio fuga, sit maiores dolores labore blanditiis velit. Iste iusto nobis ratione voluptatem aut sequi cumque enim cupiditate aliquam, quas, necessitatibus eveniet amet, veniam aperiam possimus magni vero. Rem voluptatum labore quos sequi quo ad assumenda aperiam, totam quae? Ratione, et aut, ipsam aspernatur vero odio sed id adipisci atque laboriosam accusantium consequuntur veniam repellendus repudiandae. Repellat culpa tempore mollitia ut ex veritatis distinctio cupiditate. Quibusdam eveniet nisi quam nam nihil facilis eum modi cum commodi perspiciatis ipsa itaque optio vero ut tenetur consequuntur consectetur molestiae, voluptas ratione suscipit. Sed!', 1, NULL, '2018-12-09 08:03:56', '2018-11-05 04:18:30', 5, '02', '2018'),
(3, 'dddddddd', '4', '', 2, 6, 0, '434', 0, 'eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee', NULL, 112, 'eeeeeeeeeeeeeeeew', 1, NULL, '2018-12-09 08:57:55', '2018-11-05 04:19:23', 1, '03', '2017'),
(5, 'xxxxqqqqqqqqqqq', '1', 'Commercial1.jpg', 4, 4, 0, '22', 0, 'wewwwwwwwww', NULL, 112, 'weeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee', 1, NULL, '2018-12-09 08:58:01', '2018-11-26 18:35:08', 1, '04', '2018'),
(6, 'House Dream', '6', '06-large.jpg', 5, 55, 1, '120', 1, 'Lorem ipsum doloasdasdadad amnkja ajhfo aoif asfmpklajf adsjf isdjf isdjkf sdif iuvhs 9is oisj f jssj nos', NULL, -61.38, 'Lorem ipsum doloasdasdadad amnkja ajhfo aoif asfmpklajf adsjf isdjf isdjkf sdif iuvhs 9is oisj f jssj nos ois hsdh fioushfiou siou phig dgoide iadjf oisajfoij asfa a ad fsd fsd gs gg dsg fsdg sdfg sf  Lorem ipsum doloasdasdadad amnkja ajhfo aoif asfmpklajf adsjf isdjf isdjkf sdif iuvhs 9is oisj f jssj nos ois hsdh fioushfiou siou phig dgoide iadjf oisajfoij asfa a ad fsd fsd gs gg dsg fsdg sdfg sf', 1, NULL, '2018-12-09 08:58:14', '2018-12-02 08:53:33', 1, '05', '2018'),
(7, 'My house', '2', '', 6, 85, 0, '160', 0, 'dsfsfsssssssssssssssssssssssssssssssssssssssffffffffffffffffffffffffff', NULL, 1231320, 'dsfsfsssssssssssssssssssssssssssssssssssssssffffffffffffffffffffffffff', 1, NULL, '2018-12-09 08:58:18', '2018-12-07 09:48:43', 6, '06', '2018'),
(8, 'hero home', '3', '03-large.jpg', 16, 100, 0, '220', 0, 'sd fsldkfj oisdj fosadfio sdjoifhsaiof sdoifh sadoif hasdoif sadiufh sadiuj hfbasd,kfguyhwagf jukawegfi', NULL, 12.45, 'sd fsldkfj oisdj fosadfio sdjoifhsaiof sdoifh sadoif hasdoif sadiufh sadiuj hfbasd,kfguyhwagf jukawegfi asdfguiagfuh sadijughalsdgh asiug isu uishg iushg iuasfdhg iusdhg isdfuhg idsfuhg', 1, NULL, '2018-12-15 18:52:32', '2018-12-15 16:52:32', 6, '01', '2017'),
(9, 'roro home', '9', '', 6, 60, 1, '100', 0, 'malksndokas doaj oikdajo idja dja odija odj aoidj aoijdoa nfosdnosdanoisd fosd', NULL, 7.41, 'malksndokas doaj oikdajo idja dja odija odj aoidj aoijdoa nfosdnosdanoisd fosd', 0, NULL, '2018-12-10 07:18:31', '2018-12-07 07:17:44', 6, '11', '2017'),
(10, 'Nono house', '8', 'empire-state-building-3-1000x1000.jpg', 5, 35, 0, '75', 0, 'wsikjf oiufoisfojiuhsolif sof soi fhoishg sdhgoid godh gudh odhg oshfs  hfosi hiujds gkijdsg odgoi', NULL, 1231320, 'wsikjf oiufoisfojiuhsolif sof soi fhoishg sdhgoid godh gudh odhg oshfs  hfosi hiujds gkijdsg odgoi', 1, NULL, '2018-12-09 08:58:22', '2018-12-02 10:35:48', 0, '08', '2018'),
(11, 'Salem house', '7', '01-large.jpg', 8, 120, 0, '200', 0, 'Hello I\'m Here Where were you ? Hello I\'m Here Where were you ? Hello I\'m Here Where were you ? Hello I\'m Here Where were you ? Hello I\'m Here Where w...', NULL, 1231320, 'Hello I\'m Here Where were you ? Hello I\'m Here Where were you ? Hello I\'m Here Where were you ? Hello I\'m Here Where were you ? Hello I\'m Here Where were you ?', 1, NULL, '2018-12-09 08:58:25', '2018-12-07 17:38:01', 6, '12', '2018'),
(12, 'testing', '1', '', 1, 123, 0, '1232', 0, 'hurthjrfhfg fg jfgj fgj fgj', NULL, NULL, 'hurthjrfhfg fg jfgj fgj fgj', 0, NULL, '2018-12-12 08:02:17', '2018-12-12 08:02:17', 6, '12', '2018'),
(14, 'qweqeq', '1', '', 1, 65, 0, '65', 0, 'asd asd asd asd asd', NULL, NULL, 'asd asd asd asd asd', 0, NULL, '2018-12-12 08:38:12', '2018-12-12 08:38:12', 6, '12', '2018'),
(15, 'swrwer', '1', '', 1, 21, 0, '141', 0, 'sdfvsd fds fsd fsdf sd', 124, NULL, 'sdfvsd fds fsd fsdf sd', 0, 'dada', '2018-12-13 00:28:18', '2018-12-13 00:28:18', 6, '12', '2018'),
(16, 'dسبيسي', '1', NULL, 1, 43, 0, '45', 0, NULL, NULL, NULL, 'سيبسيبسيب سيب سيب سي بسيب يس', 0, NULL, '2018-12-13 01:37:04', '2018-12-13 01:37:04', 0, NULL, NULL),
(17, 'dسبيسي', '1', '', 1, 43, 0, '45', 0, 'سيبسيبسيب سيب سيب سي بسيب يس', NULL, NULL, 'سيبسيبسيب سيب سيب سي بسيب يس', 0, NULL, '2018-12-13 01:37:06', '2018-12-13 01:37:06', 0, '12', '2018'),
(18, 'trtrtr', '1', '', 1, 66, 0, '244', 0, 'ddfgfdg fdg df gdf g fdg fd g fd g df', NULL, NULL, 'ddfgfdg fdg df gdf g fdg fd g fd g df', 0, NULL, '2018-12-13 01:43:01', '2018-12-13 01:43:01', 6, '12', '2018'),
(19, 'qw', '1', '', 1, 43, 0, '655', 0, 'fdsgdfgdf gdf gdf gdf g', NULL, NULL, 'fdsgdfgdf gdf gdf gdf g', 0, NULL, '2018-12-13 01:52:50', '2018-12-13 01:52:50', 0, '12', '2018'),
(20, 'qasdas', '1', '', 1, 35, 0, 'z1', 0, 'sadasdasd asd asd sa', NULL, NULL, 'sadasdasd asd asd sa', 0, NULL, '2018-12-13 01:57:36', '2018-12-13 01:57:36', 0, '12', '2018'),
(21, 'addads', '1', '', 1, 213, 0, '21', 0, 'asddddddddd', NULL, NULL, 'asddddddddd', 0, '233', '2018-12-13 02:16:51', '2018-12-13 02:16:51', 0, '12', '2018'),
(22, 'aseeeeeeee', '1', '', 1, 32, 0, '12', 0, 'assssssssssssssssssss', NULL, NULL, 'assssssssssssssssssss', 0, NULL, '2018-12-13 02:25:59', '2018-12-13 02:25:59', 0, '12', '2018'),
(24, 'bnnnnn', '1', '', 1, 12, 0, '34', 0, 'Hello i do it', NULL, 2222220, 'Hello i do it', 0, NULL, '2018-12-13 05:21:02', '2018-12-13 03:21:02', 6, '12', '2018'),
(29, 'Nanato ty', '1', '', 1, 123, 0, '456', 0, 'cccccccccccccccccccccc', NULL, NULL, 'cccccccccccccccccccccc', 0, NULL, '2018-12-13 09:26:15', '2018-12-13 09:26:15', 6, '12', '2018'),
(31, 'Fuck', '1', '', 1, 2221, 0, '434', 0, 'Fuck231321lksjfsajfdvcl;skmfop@!!@$$%@#^@#46', NULL, NULL, 'Fuck231321lksjfsajfdvcl;skmfop@!!@$$%@#^@#46', 0, NULL, '2018-12-13 09:33:29', '2018-12-13 09:33:29', 6, '12', '2018'),
(33, 'Al-Sharbaje', '2', '', 3, 77, 1, '89', 0, 'Hello, Dear_reader-I&#39;m testing (Now).', NULL, NULL, 'Hello, Dear_reader-I&#39;m testing (Now).', 0, NULL, '2018-12-13 12:16:10', '2018-12-13 12:16:10', 6, '12', '2018'),
(34, 'Fucker', '1', '', 1, 43, 0, '123', 0, 'skjdfhoaijsfjpi ka p qop 45sd65 46qdw qero iq09r upcaxaf&#34; lkajfdas&#34;as njkln mFucker', NULL, NULL, 'skjdfhoaijsfjpi ka p qop 45sd65 46qdw qero iq09r upcaxaf&#34; lkajfdas&#34;as njkln mFucker', 0, NULL, '2018-12-13 12:18:34', '2018-12-13 12:18:34', 6, '12', '2018'),
(35, 'upouqw', '1', '09-small.jpg', 17, 46, 0, '56757', 0, 'fdhfdhrefdgdf gf gfd gdf gdf gd fgd fgd fg', NULL, 456345000, 'hghghhghghghghghghghghghghghghghghghghghghghghghgjryuikyukyjmkgyhk', 1, 'yu', '2018-12-15 19:47:08', '2018-12-15 17:47:08', 6, '12', '2018');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `contact_name` varchar(100) NOT NULL,
  `contact_email` varchar(100) NOT NULL,
  `contact_message` text NOT NULL,
  `readIt` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `contact_name`, `contact_email`, `contact_message`, `readIt`, `created_at`, `updated_at`) VALUES
(1, 'Fami', 'Fami@gmail.com', 'Hello I wish it work very well .', 1, '2018-12-01 11:59:27', '2018-12-01 06:58:17'),
(2, 'momo', 'momo@gmail.com', 'Hay there I wish i was smart enough for that.', 1, '2018-12-01 11:59:31', '2018-12-01 08:08:17'),
(3, 'rara', 'rara@gmail.com', 'Hy There I wish you are happy.', 1, '2018-12-01 11:59:34', '2018-12-01 07:06:30'),
(4, 'rere', 'rere@gmail.com', 'Trust me I never lose, I either win or Learn .', 1, '2018-12-01 11:59:36', '2018-12-01 06:53:05'),
(8, 'assadasdasd', 'lala@gmail.com', 'asdasdasdasdasdasdasdasdasd', 1, '2018-12-01 12:29:44', '2018-12-01 10:29:44'),
(11, 'يزن', 'yazan@gmail.com', 'قطرة ماء أغلى من جوهرة ثمينة', 1, '2018-12-08 14:45:56', '2018-12-08 12:45:56'),
(12, 'جلال', 'jalal@gmail.com', 'الحمد الله كيفك انت والله مشتقلك', 0, '2018-12-01 10:36:40', '2018-12-01 10:36:40'),
(13, 'popo', 'popo@gmail.com', 'I\'m testing now.', 1, '2018-12-15 19:47:53', '2018-12-15 17:47:53'),
(14, 'wqwq', 'qww@gmail.com', 'wqqqqqqqqqqqqqqqqqqqqqeqw', 1, '2018-12-14 04:48:09', '2018-12-14 02:48:09'),
(16, 'Mahmoud', 'alhajjo.mahmoud@gmail.com', 'بسم الله الرحمن الرحيم', 0, '2018-12-15 17:54:10', '2018-12-15 17:54:10');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_10_26_082955_create_students_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('alhajjo.mahmoud@gmail.com', '$2y$10$KYZhDjdszpYsCeOsLUMRE.ZogJJetrYxPW2Dj0Xvu4y032Kkrzrk.', '2018-12-15 07:21:41'),
('yiyi@gmail.com', '$2y$10$QqDhR.7TKjqfJWew.zcyheRUZaT66k2pRxyMSmfT9ewP3VAHy7t7m', '2018-12-15 07:23:07');

-- --------------------------------------------------------

--
-- Table structure for table `sitesetting`
--

CREATE TABLE `sitesetting` (
  `id` int(11) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `namesetting` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `type` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sitesetting`
--

INSERT INTO `sitesetting` (`id`, `slug`, `namesetting`, `value`, `type`, `created_at`, `updated_at`) VALUES
(1, 'Name website', 'sitename', 'Buying & Renting', 0, '2018-12-15 15:54:53', '2018-12-15 06:55:27'),
(2, 'Mobile', 'mobile', '+963 936 597 887', 0, '2018-12-15 15:55:45', '2018-11-28 12:59:09'),
(3, 'Facebook', 'facebook', 'Facebook', 0, '2018-12-15 15:55:40', '2018-10-28 11:59:03'),
(4, 'Twitter', 'twitter', 'Twitter', 0, '2018-12-15 15:55:54', '2018-10-28 11:59:03'),
(5, 'Youtube', 'youtube', 'Youtube', 0, '2018-12-15 15:56:08', '2018-10-28 11:59:03'),
(6, 'Address', 'address', 'Al-abed street, Damascus, Syria', 0, '2018-12-15 15:56:17', '2018-11-28 19:24:27'),
(7, 'Email', 'email', 'alhajjo.mahmoud@gmail.com', 0, '2018-12-15 16:02:19', '2018-11-28 13:01:35'),
(8, 'Photo instead of user', 'no_image_user', 'https://images.freeimages.com/images/premium/large-thumbs/5487/54879668-profile-icon-male-avatar-portrait-casual-person.jpg', 0, '2018-12-15 16:03:21', '0000-00-00 00:00:00'),
(9, 'Image instead of building', 'no_image', 'http://www.gladessheriff.org/media/images/most%20wanted/No%20image%20available.jpg', 0, '2018-12-15 16:03:46', '2018-11-23 13:21:57'),
(10, 'Site rights', 'footer', 'Programming by HOOOT', 0, '2018-12-15 16:10:02', '2018-12-15 14:10:02'),
(11, 'Key words', 'keywords', 'put here words for helping search in Google.', 1, '2018-12-15 16:10:02', '2018-12-15 14:10:02'),
(12, 'website description', 'description', 'description', 1, '2018-12-15 16:04:57', '2018-10-28 11:59:03');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `admin` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `image`, `created_at`, `updated_at`, `admin`) VALUES
(1, 'Nader', 'nader@sad.com', NULL, '$2y$10$5H7tRd/zCGis6cf2uY8Np./Rkk4wpFqrtO.dvLwsnNWscQof3MHbm', 'CNQobJpecgRS0FjqEjSxIlq0yOHKk4Tb0EW86Z3cQ1v4R8uPnXuFPJiEtMvh', '', '2018-10-26 09:06:19', '2018-10-26 09:06:19', 1),
(3, 'Fadi', 'fadi@sad.com', NULL, '$2y$10$9PCTveThoqUSGXOlpHgWAuARdixb1PO/05RQW9we0WH0Znd7SEG9K', NULL, '', '2018-10-27 16:27:27', '2018-10-27 16:27:27', 0),
(4, 'Ahmad', 'Ahmad@gmail.com', NULL, '$2y$10$qzxOKdNqEWuqh6bXee2cVeMSWot5Zhx.XOsTbsgOduVoej7QK3AZK', NULL, '', '2018-10-27 16:28:04', '2018-10-27 16:28:04', 0),
(5, 'Khaled', 'Khaled@gmail.com', NULL, '$2y$10$IIEL/Innrd4N9cysw/vZPOZK./ma0BZizjN5EuBXQQKA8FXIJDEfO', NULL, '', '2018-10-27 16:28:50', '2018-10-27 16:28:50', 0),
(6, 'Mahmoud', 'alhajjo.mahmoud@gmail.com', NULL, '$2y$10$gB./l8iA9gkwG4R3GlwWDOLnRVuQDsOKSV3iFoihMfom1m3.VyJSq', 'D5HB6nD2yjTq0ASghaPQ0YieIaSnYMMYlST8zQhJgB3eqbw8lDX1byn8f3ac', 'download.jpg', '2018-11-12 12:34:04', '2018-12-11 08:16:22', 1),
(7, 'Lalad', 'LALA@gmail.com', NULL, '$2y$10$RPg6Y332ufTTU7.Lt0oHOeEiWOagjzTfc9BUZgwfXr/FPdjeDLh/q', '2FKVbI2hAfQVismlWdXQjwKbmtaxImcKmVFQOwzhxCT4se5fb5sWNcao9Ysh', 'p; 044.jpg', '2018-12-01 10:07:58', '2018-12-10 06:41:53', 0),
(8, 'Shadi', 'Shadi@gmail.com', NULL, '$2y$10$kOToLxvu8uI5y.nllpUp.O4dqBxcorkEOPkZ2HjKrfBsZ.lcOnKje', 'MXGVeV5zjRSxtNldzZBqOQleVEmmbUDES8xInDZZWbkOoqLDGhvEGqtOzmkW', '', '2018-12-04 04:55:30', '2018-12-04 04:55:30', 0),
(9, 'Rami', 'Rami@gmail.com', NULL, '$2y$10$c0shu9cocrYRkIeygGibF.0p1PAsuUaiuzpSLTSN.kDShqQJhVkXO', 'l0rJTRVRaiMgIWbilsZDc8wiJFUprSWdZv6T8mTTkskiefI9xkvMyl6HyEGM', '', '2018-12-05 14:18:48', '2018-12-05 14:18:48', 0),
(10, 'Jmale', 'Jmal@gmail.com', NULL, '$2y$10$.H1fXEuKP/6PnxS8C0l01uYQ7z0e6.nLw6cQI8.BGkDf9kPAH5Lj.', NULL, 'www.YTS.AM.jpg', '2018-12-10 06:27:01', '2018-12-10 06:34:58', 0),
(11, 'Sham', 'Sham@gmail.com', NULL, '$2y$10$RUrXlevGE/ZM5bjV1DUeGusHMqw1V5iDVseUOB.dAE8ylGgZDExcq', 'hcJFqjlv0GxAcC37aHbpFDlPWSWkSlVBwJTPjvRAgphCbCf7W0rIPQSGaNLN', '', '2018-12-10 06:57:11', '2018-12-10 06:57:11', 0),
(12, 'Ema', 'Ema@gmail.com', NULL, '$2y$10$U8xuAKsgNzx36D/.dS0b6e2b96vXOzx13AwgoYAWfAONxJx4tLjO2', 'ItS2THgetruLbIjDDGyVPpxM8IZQi70vFnyJAF34QZA5J3Wsge7L35PUoMmV', '', '2018-12-10 07:51:25', '2018-12-10 07:51:25', 0),
(13, 'Rama', 'rama@gmail.com', NULL, '$2y$10$Z2HK60NvRHoz2cczVegx2.Z3j.esBpj7d4/oz//9swF08ORXO9wvO', 'lYCEkfcmr1wNw1SUkNR63AI4V3nKMBgBzpFSyJOiqCT5KEBy66I5WMRvHeFt', '', '2018-12-10 07:54:57', '2018-12-10 07:54:57', 0),
(14, 'Rawda', 'Rawa@gmail.com', NULL, '$2y$10$MC1oFIA98oxL53Hq7Zc6nu1MJrN.IQd0O/F09wM3YraHX22s8OlQi', NULL, '11-2D7BEBD9-623191-480-100.jpg', '2018-12-10 08:19:33', '2018-12-10 09:03:22', 0),
(15, 'pop', 'pop@gmail.com', NULL, '$2y$10$kKYVSWJUZbfwU938dpEnLeWD8SYe5Pvy0rbL9tIiKcVFYnXgRtol.', 'nBfsROLBUJ83DbrDaeXeWdm7TPt3WrwyWljYT621O9cKW1d6rxS27bbLckbK', '', '2018-12-13 00:35:57', '2018-12-13 00:35:57', 0),
(16, 'yoyo', 'yoyo@gmail.com', NULL, '$2y$10$o/0/jVTHazuLY5r1D/Jozu4beyOo9FBIqUJaq1lBmNaVfJoRhz8e2', 'IKYLjobiy9SVul1CdL2SY4aWoXlCUHqP7DRDEVroxysO3Hl7LJCYcoqYRkuw', '', '2018-12-13 00:59:50', '2018-12-13 00:59:50', 0),
(17, 'qoqo', 'qoqo@gmail.com', NULL, '$2y$10$gs3gbeuN3pz8dWbUIIyxz..02pFVsCHZZgKrefrbIzr/N41Sc1zKG', '9pQSU6ln354l10OyA5r0nFzbwKHPXRfzfEcnZ28DTszp9flXCN9egKLdPh2u', '', '2018-12-13 01:13:48', '2018-12-13 01:13:48', 0),
(18, 'yiyi', 'yiyi@gmail.com', NULL, '$2y$10$Ee6AqiExLW0TcfND2V9bPOENiNlef318Rd1v9cstRxXWsO2sV7l6m', 'KScRlMWRGv49g7rC3qPIAljkhNr4k3MQuDMZqaTI7wfXJawAtwYRyhC1vV1l', '', '2018-12-15 05:27:30', '2018-12-15 05:27:30', 0),
(19, 'soy', 'soy@gmail.com', NULL, '$2y$10$a5rw7KqVEzatuMQIjRXJZ.TVnhZ8A5J/DXBrUJvtL.QpeDOKZtiDq', NULL, '', '2018-12-15 15:26:45', '2018-12-15 15:26:45', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bu`
--
ALTER TABLE `bu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `sitesetting`
--
ALTER TABLE `sitesetting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bu`
--
ALTER TABLE `bu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sitesetting`
--
ALTER TABLE `sitesetting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
