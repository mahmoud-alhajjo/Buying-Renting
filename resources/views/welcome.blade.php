@extends('layouts.app') 
@section('title') Welcome
@endsection
 
@section('header')
@endsection
 
@section('content')
<!-- Header -->
<header id="header">
  <div class="intro">
    <div class="overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-md-offset-2 intro-text">
            <h1>{{getSetting()}}</h1>
            <p>
              Waht are you looking for selling or rents ?<br> Trust us , You'll find it here
            </p>
            <a href="{{url('/user/create/building')}}" class="btn btn-custom btn-lg page-scroll">Add Building Free</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>

{{--
<ul class="cd-items cd-container">
  <div class="section-title">
    <h2 style="font-size: 32px;">Latest Ads </h2>
  </div>
  <div class="clearfix"></div>
  @foreach(\App\Bu::where('bu_status',1)->get() as $bu)
  <li class="cd-item effect3">
    <img src="{{checkIfImageIsexist($bu->image)}}" alt="{{$bu->name}}" title="{{$bu->name}}" class="img-responsive" style="width:257.391px;height:289.563px;">
    <a href="#0" class="cd-trigger" title="{{$bu->name}}" data-id="{{$bu->id}}">Quick View</a> @endforeach
  </li>
  <!-- cd-item -->
</ul>
<!-- cd-items -->

<div class="cd-quick-view">
  <div class="cd-slider-wrapper">
    <ul class="cd-slider">
      <li class="selected"><img class="imagebox img-responsive" src="" alt="{{$bu->name}}" title="{{$bu->name}}" style="height:350px;width:300;"></li>
    </ul>
  </div>
  <!-- cd-slider-wrapper -->

  <div class="cd-item-info">
    <h2 class="titlebox"></h2>
    <p class="disbox"></p>
    <ul class="cd-item-action">
      <li><a href="" class="pricebox">Add to cart</a></li>
      <li><a href="" class="morebox">Read more</a></li>
    </ul> --}}

    <!-- Gallery Section -->
    <!-- Gallery Section -->
    <div id="portfolio">
      <div class="container">
        <div class="section-title">
          <h2>Latest Ads</h2>
        </div>
        <div class="row">
          <div class="portfolio-items">
            @foreach(\App\Bu::where('bu_status',1)->take('9')->orderBy('id', 'desc')->get() as $bu)
            <div class="col-sm-6 col-md-4 col-lg-4">
              <div class="portfolio-item">
                <div class="hover-bg">
                  <a href="{{checkIfImageIsexist($bu->image)}}" title="{{$bu->bu_name}}" data-lightbox-gallery="gallery1">
                    <div class="hover-text">
                      <h4>{{$bu->bu_name}}</h4>
                    </div>
                    <img src="{{checkIfImageIsexist($bu->image)}}" style="width:360px;height:240px;" alt="{{$bu->bu_name}}" class="img-responsive">                    </a>
                </div>
              </div>
            </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
    <!-- Contact Section -->
    <div id="contact">
      <div class="container">
          <div class="col-md-12">
              <div class="row">
                  <div class="section-title">
                      <h2>Search</h2>
                      <p>Search for what you needing easily , trust us</p>
                  </div>
                  {!! Form::open(['url'=>'/search','method'=>'get'])!!}
                      <div class="row">
                          <div class="col-md-4">
                              <div class="form-group searchw">
                                  {!! Form::text('bu_price_from',null,['class'=>'form-control ','placeholder'=>'Price Building from'])!!}
                              </div>
                          </div>
  
                          <div class="col-md-4">
                              <div class="form-group searchw">
                                  {!! Form::text('bu_price_to',null,['class'=>'form-control ','placeholder'=>'Price Building to'])!!}
                              </div>
                          </div>
                          <div class="col-md-4">
                              <div class="form-group searchw">
                                  {!! Form::select('bu_place',bu_place(),null,['class'=>'form-control js-example-basic-single ','placeholder'=>'Place'])!!}
                                </div>
                          </div>
                          <div class="col-md-4">
                              <div class="form-group searchw">
                                  {!! Form::select("rooms",rooms(),null,['class'=>"form-control ",'placeholder'=>"rooms"])!!}
                                </div>
                          </div>
                          <div class="col-md-4">
                              <div class="form-group searchw">
                                  {!! Form::select("bu_type",bu_type(),null,['class'=>"form-control",'placeholder'=>"Type"])!!}
                              </div>
                          </div>
                          <div class="col-md-4">
                              <div class="form-group searchw">
                                  {!! Form::select("bu_rent",bu_rent(),null,['class'=>"form-control ",'placeholder'=>"ownership"])!!}
                                </div>
                          </div>
      
                        
                         
                          <div class="col-md-4">
                              <div class="form-group searchw" style="width:100%;">
                                      {!! Form::text("bu_square",null,['class'=>"form-control",'placeholder'=>"Square"])!!}
                                </div>
                          </div>
  
                      </div>
                      <button type="submit" class="btn btn-custom btn-lg" style="clear:both;display:block">Search</button>

                    </form>
              </div>
          </div>
          <div class="col-md-12">
            <div class="row">
              <div class="social">
                <ul>
                  <li><a href="{{getSetting('facebook')}}"><i class="fa fa-facebook"></i></a></li>
                  <li><a href="{{getSetting('twitter')}}"><i class="fa fa-twitter"></i></a></li>
                  <li><a href="{{getSetting('youtube')}}"><i class="fa fa-youtube"></i></a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection
 
@section('footer')
      <script>
        function urlHome(){
    return '{{ Request::root() }}';
  }
  function noImageUrl(){
    return '{{getSetting('no_image')}}';
  }
      </script>
@endsection