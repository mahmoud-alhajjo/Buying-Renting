@extends('layouts.app') 
@section('title') عضوية جديدة
@endsection
 
@section('content')
<!-- login Section -->
<div id="login">
    <div class="container">
        <div class="col-md-8">
            <div class="row">
                <div class="section-title">
                    <h2>Register</h2>
                    <p>Please fill out the form below to be member.</p>
                </div>
                <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data" role="form">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input id="name" type="text" placeholder="Name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name"
                                    value="{{ old('name') }}" required autofocus> @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span> @endif
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <input id="email" type="email" placeholder="E-Mail Address" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                    name="email" value="{{ old('email') }}" required> @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span> @endif
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <input id="password" type="password" placeholder="Password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                    name="password" required> @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span> @endif
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input id="password-confirm" type="password" placeholder="Confirm Password" class="form-control" name="password_confirmation"
                                    required>
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>

                        <div class="col-md-6 custom-file">
                            <div class="form-group" style="position: relative;">
                                <i class="fa fa-image fa-4x" style="cursor:pointer;"></i><span style="position: absolute;margin-left: 15px;top: 18px;"></span> {!!
                                Form::file('image',['class'=>'','style'=> 'display:none']) !!} @if ($errors->has('image'))
                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('image') }}</strong>
                                                </span> @endif
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                    </div>


            </div>
        </div>


        <button type="submit" class="btn btn-custom btn-lg" style="clear:both;display:block">Register</button>

        </form>
    </div>
</div>
@endsection

@section('footer')
<script>
$('i.fa-image').on('click',function(){
    $('input').trigger('click');
});
$('.custom-file input[type="file"]').change(function () {
       $(this).prev('span').text($(this).val());
});
</script>
@endsection