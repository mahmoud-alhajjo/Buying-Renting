@extends('layouts.app') 
@section('title') تسجيل الدخول
@endsection
 
@section('content')
<!-- login Section -->
<div id="login">
    <div class="container">
        <div class="col-md-8">
            <div class="row">
                <div class="section-title">
                    <h2>Log in</h2>
                    <p>Please fill out the form below to login.</p>
                </div>
                <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="E-Mail Address"
                                    value="{{ old('email') }}" required autofocus> @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span> @endif
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Password"
                                    name="password" required> @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span> @endif
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old( 'remember') ? 'checked' : '' }}>
                            <label class="form-check-label" for="remember" style="font-size: 14px;color: lightblue;padding-right:10px">
                                {{ __('Remember Me') }}
                            </label>
                            <p class="help-block text-danger"></p>
                        </div>
                    </div>
            </div>
           
            <button type="submit" class="btn btn-custom btn-lg">Login</button>
            {{-- <a class="btn btn-link" href="{{ route('password.request') }}" style="text-decoration: none;color: #c13a3a;">
                        {{ __('Forgot Your Password?') }}
                    </a> --}}
            </form>
        </div>
    </div>
</div>
</div>
@endsection