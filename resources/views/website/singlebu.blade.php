@extends('layouts.app') 
@section('title') العقار {{$buInfo->bu_name}}
@endsection
 
@section('header') {!! Html::style('cus/buAll.css')!!}
<script src="http://maps.googleapis.com/maps/api/js"></script>
<script>
        // var myCenter=new google.maps.LatLng(51.508742,-0.120850);
        var myCenter=new google.maps.LatLng({{ $buInfo->bu_longitude}},{{$buInfo->bu_latitude}});
        var marker;
        
        function initialize()
        {
        var mapProp = {
          center:myCenter,
          zoom:5,
          mapTypeId:google.maps.MapTypeId.ROADMAP
          };
        
        var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
        
        var marker=new google.maps.Marker({
          position:myCenter,
          animation:google.maps.Animation.BOUNCE
          });
        
        marker.setMap(map);
        }
        
        google.maps.event.addDomListener(window, 'load', initialize);
        </script>
@endsection
 
@section('content')
<div class="container">
    <div class="row justify-content-center">
    @include('website.page')
        <div class="col-md-9">
            <ol class="breadcrumb" style="background:#dddedd;margin-left:13px;margin-right:13px;">

                <li><a href="{{url('/')}}">Home</a></li>
                <li><a href="{{url('/ShowAllBuilding')}}">All Building</a></li>
                <li><a href="{{url('/singleBuilding'.'/'.$buInfo->id)}}">{{$buInfo->bu_name}}</a></li>
            </ol>

            <div class="profile-content">
                <h1 style="color: #1f386e;">{{$buInfo->bu_name}}:</h1>
                <br>
                <div class="btn-group" role="group">
                    <a href="{{url('/search?bu_price='.$buInfo->bu_price)}}" class="btn btn-default" style="font-weight: bold;">Price:{{$buInfo->bu_price}}</a>
                    <a href="{{url('/search?square='.$buInfo->rooms)}}" class="btn btn-default" style="font-weight: bold;">Rooms:{{$buInfo->rooms}}</a>
                    <a href="{{url('/search?square='.$buInfo->square)}}" class="btn btn-default" style="font-weight: bold;">Square:{{$buInfo->bu_square}}</a>
                    <a href="{{url('/search?bu_place='.$buInfo->bu_place)}}" class="btn btn-default" style="font-weight: bold;">Place:{{bu_place()[$buInfo->bu_place]}}</a>
                    <a href="{{url('/search?bu_type='.$buInfo->bu_type)}}" class="btn btn-default" style="font-weight: bold;">Type:{{bu_type()[$buInfo->bu_type]}}</a>
                    <a href="{{url('/search?bu_rent='.$buInfo->bu_rent)}}" class="btn btn-default" style="font-weight: bold;">Ownerships:{{bu_rent()[$buInfo->bu_rent]}}</a>
                </div>
                <hr style="background: #7c9ba9;"> 
                <img src="{{ checkIfImageIsexist($buInfo->image)}}" alt="Loading" class="img-responsive" style="border-bottom: solid 1px #dde6ea;">
<br>
                <p class="lead" style="color: #1f386e;overflow-wrap:break-word;">
                    {!! ($buInfo->bu_large_dis) !!}
                </p>

                <div id="googleMap" style="width:100%;height:380px;"></div>

            </div>
            <br>
            <h3 style="color: #1f386e;margin-left: 16px;">Same of type :</h3><br>
                @include('website.sharefile',['bu'=> $sem_rent])
                {{-- @include('website.sharefile',['bu'=> $sem_type]) --}}
        </div>
    </div>
</div>
@endsection
@section('footer')
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5bfd6e1f1f4107a4"></script>
@endsection