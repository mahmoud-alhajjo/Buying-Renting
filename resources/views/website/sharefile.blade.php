@if(count($bu) > 0)

  @foreach($bu as $key => $b)
    {{-- @if($key % 3 == 0  && $key != 0 )
    <div class="clearfix"></div>
    @endif --}}
    <div class="col-lg-4  col-sm-6 fix">
      <div class="thumbnail">
      <img src="{{ checkIfImageIsexist($b->image)}}" alt="Loading" class="img-responsive" style="border-bottom: solid 1px #dde6ea;height:242.484px;width:242.484px;">
        <div class="caption">
          <h4 class="pull-right price"><i class="fa fa-dollar"></i>{{$b->bu_price}}</h4>
          <h4><a href="#">{{$b->bu_name}}</a></h4>
          <p style="overflow-wrap: break-word;height:42px">{{str_limit($b->bu_small_dis,50)}}</p>
         <hr style="margin:0 !important;">
         <span class=" test" ><span style="color:#d02222">square  :</span>   {{$b->bu_square}}</span>
         <span class=" test" ><span style="color:#d02222">place  :</span>   {{bu_place()[$b->bu_place]}}</span>
         <span class=" test" ><span style="color:#d02222">ownership  :</span>   {{bu_rent()[$b->bu_rent]}}</span>
         <span class=" test" ><span style="color:#d02222">type  :</span>   {{bu_type()[$b->bu_type]}}</span>
          {{-- <div class="clearfix"></div> --}}
          <div class="clearfix"></div>
          <hr style="margin:0 !important;">
          {{-- <span class="pull-right">ownership:{{bu_rent()[$b->bu_rent]}}</span>
          <span class="pull-right">place:{{bu_place()[$b->bu_palce]}}</span> --}}
        </div>
       
        <div class="space-ten"></div>
        <div class="btn-ground text-center">
        @if($b->bu_status == 1)
        <a href="{{url('/singleBuilding'.'/'.$b->id)}}"> <button type="button" class="btn btn-primary"><i class="fa fa-shopping-cart"></i> More details</button></a>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#{{$b->id}}"><i class="fa fa-search"></i> Quick View</button>
        @else
        <a href="{{url('/singleBuilding'.'/'.$b->id)}}"> <button type="button" class="btn btn-danger" style="font-size: 12.5px;"><i class="fa fa-shopping-cart"></i> Wait approve</button></a>
        <a href="{{url('/edit/user/building/'.$b->id)}}"> <button type="button" class="btn btn-warning" style="font-size: 12.5px;" ><i class="fa fa-shopping-cart"></i> Edit Building</button></a>
        @endif
        </div>
        <div class="space-ten"></div>
      </div>
    </div>

    
    <div class="modal fade product_view" id="{{$b->id}}">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <a href="#" data-dismiss="modal" class="class pull-right"><span class="fa fa-remove"></span></a>
                    <h3 class="modal-title">{{$b->bu_name}}</h3>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6 product_img">
                            <img src={{checkIfImageIsexist($b->image)}} class="img-responsive">
                        </div>
                        <div class="col-md-6 product_content">
                            <h4>Number of rooms ( <span>{{$b->rooms}} )</span></h4>
                            <p style="word-wrap: break-word;">{{$b->bu_small_dis}}</p>
                            <h3 class="cost"><span class="fa fa-dollar"></span> {{$b->bu_price}}</h3>
                            <a href="{{url('/singleBuilding'.'/'.$b->id)}}"> <button type="button" class="btn btn-danger"><i class="fa fa-shopping-cart"></i> More details</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

  @endforeach


@else
<div class="alert alert-danger" style="    width: 96%;
margin-left: 2%;">
 There are currently no Buildings. 
</div>
@endif