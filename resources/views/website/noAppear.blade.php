@extends('layouts.app') 
@section('title') {!! $messagetitle !!}
@endsection
 
@section('header') {!! Html::style('cus/buAll.css')!!}
@endsection
 
@section('content')
<div class="container">
    <div class="row justify-content-center">
    @include('website.page')

        <div class="col-md-9">
                <ol class="breadcrumb" style="background:#dddedd;margin-left:13px;margin-right:13px;">

                        <li><a href="{{url('/')}}">Home</a></li>
                        <li><a href="{{url('/ShowAllBuilding')}}">All Building</a></li>
                        <li><a href="{{url('singleBuilding/'.$buInfo->id)}}"> {{$buInfo->bu_name}}</a></li>
        
                    </ol>
                    <div class="alert alert-danger" style="    width: 96%;
                    margin-left: 2%;">
                            {!! $messagebody !!}
                    </div>
        </div>
        <br>
        <br>

    </div>
</div>
</div>
</div>
@endsection
 
@section('footer')

@endsection