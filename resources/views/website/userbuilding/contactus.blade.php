@extends('layouts.app') 
@section('title') Contuct Us
@endsection
 
@section('content')
<!-- login Section -->
<div id="contact">
    <div class="container">
        <div class="col-md-8">
            <div class="row">
                <div class="section-title">
                    <h2>Get In Touch</h2>
                    <p>Please fill out the form below to send us an email and we will get back to you as soon as possible.</p>
                </div>
                <form method="POST" action="{{ url('/contactus')}}">
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input id="contact_name" type="text" placeholder="Name" class="form-control{{ $errors->has('contact_name') ? ' is-invalid' : '' }}"
                                    name="contact_name" value="{{ old('contact_name') }}" required autofocus>                                @if ($errors->has('contact_name'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('contact_name') }}</strong>
                                    </span> @endif
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input id="contact_email" type="email" placeholder="Eamil" class="form-control{{ $errors->has('contact_email') ? ' is-invalid' : '' }}"
                                    name="contact_email" value="{{ old('contact_email') }}" required autofocus>                                @if ($errors->has('contact_email'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('contact_email') }}</strong>
                                    </span> @endif
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <textarea id="contact_message" rows="4" type="text" placeholder="Message" class="form-control{{ $errors->has('contact_message') ? ' is-invalid' : '' }}"
                            name="contact_message" value="{{ old('contact_message') }}" required autofocus></textarea>                        @if ($errors->has('contact_message'))
                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('contact_message') }}</strong>
                                        </span> @endif
                        <p class="help-block text-danger"></p>
                    </div>
                    <div id="success"></div>
                    <button type="submit" class="btn btn-custom btn-lg">Send Message</button>
                </form>
            </div>
        </div>
        <div class="col-md-3 col-md-offset-1 contact-info">
            <div class="contact-item">
                <h4>Contact Info</h4>
                <p><span>Address</span>{{getSetting('address')}}</p>
            </div>
            <div class="contact-item">
                <p><span>Phone</span>{{getSetting('mobile')}}</p>
            </div>
            <div class="contact-item">
                <p><span>Email</span>{{getSetting('email')}}</p>
            </div>
        </div>
    </div>
</div>
@endsection