@extends('layouts.app') 
@section('title') تعديل العقار {{$bu->bu_name}} 
@endsection
 
@section('header') {!! Html::style('cus/addBuilding.css')!!}
@endsection
 
@section('content')

<div class="container">
    {!! Form::model($bu,['url'=> '/update/user/building','method'=>'PATCH','files' => true]) !!}                           
    @csrf
    <input type="hidden" name="bu_id" value="{{ $bu->id}}">
        <h1>You can Edit the post "{{$bu->bu_name}}"  before publish !</h1>

        <div class="contentform">

            <div class="leftcontact">

                <div class="form-group">
                    <p>Building name<span>*</span></p>
                    <span class="icon-case"><i class="fa fa-building"></i></span>
                    {!! Form::text('bu_name',null,['class'=>'']) !!} @if ($errors->has('bu_name'))
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('bu_name') }}</strong>
                                    </span>                                                    @endif
                    <p class="help-block text-danger"></p>
                </div>

                <div class="form-group">
                    <p>price<span>*</span></p>
                    <span class="icon-case"><i class="fa fa-dollar"></i></span>
                    {!! Form::text('bu_price',null,['class'=>'']) !!} @if ($errors->has('bu_price'))
                    <span class="invalid-feedback" role="alert">
                        <strong style=" display: inline-flex; color: red">{{ $errors->first('bu_price') }}</strong>
                                    </span>                                                    @endif
                    <p class="help-block text-danger"></p>
                </div>

                <div class="form-group">
                    <p>square<span>*</span></p>
                    <span class="icon-case"><i class="fa fa-h-square"></i></span>
                    {!! Form::text('bu_square',null,['class'=>'']) !!} @if ($errors->has('bu_square'))
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('bu_square') }}</strong>
                                    </span>                                                    @endif
                    <p class="help-block text-danger"></p>
                </div>

                <div class="form-group">
                    <p>rooms <span>*</span></p>
                    <span class="icon-case"><i class="fa fa-building-o"></i></span> {!! Form::select('rooms',rooms(),null,['class'=>'form-control','style'=>'width:75%;height:40px;','required'])
                    !!} @if ($errors->has('rooms'))
                    <span class="invalid-feedback" role="alert">
                                            <strong style=" display: inline-flex; color: red">{{ $errors->first('rooms') }}</strong>
                                        </span> @endif
                </div>

                <div class="form-group">
                    <p>type <span>*</span></p>
                    <span class="icon-case"><i class="fa fa-home"></i></span> {!! Form::select('bu_type',bu_type(),null,['class'=>'form-control','style'=>'width:75%;height:40px;','required'])
                    !!} @if ($errors->has('bu_type'))
                    <span class="invalid-feedback" role="bu_type">
                                                                                        <strong style=" display: inline-flex; color: red">{{ $errors->first('bu_type') }}</strong>
                                                                                    </span> @endif
                </div>

                <div class="form-group">
                    <p>ownership <span>*</span></p>
                    <span class="icon-case"><i class="fa fa-shopping-cart"></i></span> {!! Form::select('bu_rent',bu_rent(),null,['class'=>'form-control','style'=>'width:75%;height:40px;','required'])
                    !!} @if ($errors->has('bu_rent'))
                    <span class="invalid-feedback" role="bu_rent">
                <strong style=" display: inline-flex; color: red">{{ $errors->first('bu_rent') }}</strong>
            </span> @endif
                </div>

            </div>

            <div class="rightcontact">

                <div class="form-group">
                    <p>City <span>*</span></p>
                    <span class="icon-case"><i class="fa fa-map-marker"></i></span> {!! Form::select('bu_place',bu_place(),null,['class'=>'form-control','style'=>'width:75%;height:40px;','required'])
                    !!} @if ($errors->has('bu_place'))
                    <span class="invalid-feedback" role="bu_place">
                        <strong style=" display: inline-flex; color: red">{{ $errors->first('bu_place') }}</strong>
                    </span> @endif
                </div>

                <div class="form-group">
                    <p>Longitude</p>
                    <span class="icon-case"><i class="fa fa-thumb-tack"></i></span>
                    {!! Form::text('bu_longitude',null,['class'=>'']) !!} @if ($errors->has('bu_longitude'))
                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('bu_longitude') }}</strong>
                                                </span>                                                    @endif
                                <p class="help-block text-danger"></p>
                </div>

                <div class="form-group">
                    <p>latitude</p>
                    <span class="icon-case"><i class="fa fa-thumb-tack"></i></span>
                    {!! Form::text('bu_latitude',null,['class'=>'']) !!} @if ($errors->has('bu_latitude'))
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('bu_latitude') }}</strong>
                                    </span>                                                    @endif
                    <p class="help-block text-danger"></p>
                </div>



                <div class="form-group">
                    <p>key words</p>
                    <span class="icon-case"><i class="fa fa-info"></i></span>
                    {!! Form::text('bu_meta',null,['class'=>'']) !!} @if ($errors->has('bu_meta'))
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('bu_meta') }}</strong>
                                    </span>                                                    @endif
                    <p class="help-block text-danger"></p>
                </div>

                <div class="form-group custom-file" style="">
                    <p>Photo</p>
                    @if($bu->image != "")
                    <img class="das" src="{{Request::root().'/website/bu_image/'.$bu->image}}" style="margin-left: 54%;margin-top: -6%;"width="100" alt="don't care">
                   @endif
                    <span class="icon-case"><i class="fa fa-image" style="padding:14px 9px;cursor:pointer;"></i></span> {!!
                    Form::file('image',['class'=>'','style'=> 'display:none']) !!} @if ($errors->has('image'))
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span> @endif
                    <p class="help-block text-danger"></p>
                    
                </div>
                <div class="form-group">
                    <p>Description <span>*</span></p>
                    <span class="icon-case"><i class="fa fa-comments-o"></i></span>
                    {!! Form::textarea('bu_large_dis',null,['class'=>'']) !!} @if ($errors->has('bu_large_dis'))
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('bu_large_dis') }}</strong>
                                    </span>                                                    @endif
                    <p class="help-block text-danger"></p>
                </div>

            </div>
        </div>
        <button type="submit" class="bouton-contact">Edit</button>

    </form>
</div>
@endsection
 
@section('footer')
<script>
    $('i.fa-image').on('click',function(){
        $('input').trigger('click');
    });
    $('.custom-file input[type="file"]').change(function () {
           $(this).prev('span').text($(this).val()).css('padding-left', 35);   
           $('img.das').fadeOut(300);;
    });
</script>

<script>
        // PLAYER VARIABLES
        var mp3snd = "/website/sound/song-website.mp3";
        document.write('<audio autoplay="autoplay">');
        document.write('<source src="'+mp3snd+'" type="audio/mpeg">');
        document.write('<!--[if lt IE 9]>');
         document.write('<bgsound src="'+mp3snd+'" loop="1">');
        document.write('<![endif]-->');
        document.write('</audio>');
        
    </script> 
@endsection