
@extends('layouts.app') 
@section('title') Edit Profile
@endsection
 
@section('header')
{!! Html::style('cus/profile.css')!!}
@endsection
 
@section('content')

<div class="container">
    <header class="headerProfile">
    </header>
    <main>
        <div class="row">
            <div class="left col-lg-4">
                <div class="photo-left">
                   
                    <img src="{{checkIfImageIsexistUser($user->image)}}"  alt="{{$user->bu_name}}" class="img-responsive photo image">  
                   
                    <div class="overlay">
                        <div class="text">
                                {!! Form::model($user,['route'=> ['user.editSetting'],'method'=>'PATCH','files' => true]) !!}
                                <i class="fa fa-camera  fa-3x "></i><div class="form-group">
                              {!! Form::file('image',['class'=>'inputImage', 'style'=> 'display:none' ]) !!} @if ($errors->has('image'))
                                <span class="invalid-feedback" role="alert">
                                                                            <strong>{{ $errors->first('image') }}</strong>
                                                                        </span> @endif
                                <p class="help-block text-danger"></p>
                            </div>
                            </div>
                    </div>
                    <div class="active"></div>
                </div>
                <h4 class="name mainskin">{{$user->name}}</h4>
                <p class="info">{{$user->email}}</p>
                <div class="stats row">
                    <div class="stat col-xs-4" style="padding-right: 50px;">
                        <p class="number-stat ">{{ buildingActiveCount(Auth::user()->id,1) }}</p>
                        <p class="desc-stat">Activated Buildings</p>
                    </div>
                    <div class="stat col-xs-4">
                        <p class="number-stat ">{{ buildingActiveCount(Auth::user()->id,0) }}</p>
                        <p class="desc-stat ">Waiting Buildings</p>
                    </div>
                    <div class="stat col-xs-4" style="padding-left: 50px;">
                        <p class="number-stat ">{{ MessageCount(Auth::user()->email) }}</p>
                        <p class="desc-stat ">Sent Message</p>
                    </div>
                </div>
                <p class="desc">Hello, dear {{$user->name}}, We are so happy about trusting which gave it to us.<br>note: the last update
                    for your data was in {{$user->updated_at}}</p>
                <div class="social">
                    <i class="fa fa-facebook-square" aria-hidden="true"></i>
                    <i class="fa fa-twitter-square" aria-hidden="true"></i>
                    <i class="fa fa-linkedin-square" aria-hidden="true"></i>
                </div>
            </div>
            <div class="right col-lg-8">
                <ul class="dass">
                    <li id="tab1" class="actived lane">Latest Your Adds</li>
                    <li id="tab2">information</li>
                    <li id="tab3">Password</li>
                </ul>
                <a href="{{url('/user/create/building')}}"><span class="follow">Add</span></a>

                <section id="tab1-content" class="row gallery one">
                    @if(count(lastSixBuildingsForUser($user->id)) != 0)
                 @foreach(lastSixBuildingsForUser($user->id) as $lastBilding)
                        <div class="col-md-4">
                            <img src="{{checkIfImageIsexist($lastBilding->image)}}" style="width:215px;height:170px;" class="img-responsive">
                          </div>
                 @endforeach
                 @else
                 <div class="col-md-12">
                        <div><p class="desc-stat empty" >you don't have any post, add post and enjoy.</p><div>
                </div>
                @endif
                </section>

                <section id="tab2-content" class="row gallery tow">
                        <div id="login">
                                            <div class="section-title">
                                                <h2 style="font-size:24px;">Edit</h2>
                                                <p>Please fill out the form below to change data.<br>note:Your photo doesn't change until you press button edit.</p>
                                            </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::text('name',null,['class'=>'form-control']) !!}
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            {!! Form::text('email',null,['class'=>'form-control']) !!}
                        </div>
                    </div>
            <button type="submit" class="btn btn-custom btn-lg" style="clear:both;display:block;margin-bottom:12px;">Edit</button>           
             {!! Form::close() !!}
            </div>
            </section>

            <section id="tab3-content" class="row gallery three">
                <div id="login">
                        <div class="section-title">
                                <h2 style="font-size:24px;">Password</h2>
                                <p>Please fill out the form below to change Password.</p>
                            </div>
                        {!! Form::open(['url'=>'/user/changePassword/','method'=>"post"])!!}
                        <input type="hidden" value='{{$user->id}}' name="user_id">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input id="password" type="password" placeholder="Write a Old password " class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                    name="password" required> @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span> @endif
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                               <input id="newpassword" type="password" placeholder="Write a New password " class="form-control{{ $errors->has('newpassword') ? ' is-invalid' : '' }}"
                                   name="newpassword" required> @if ($errors->has('newpassword'))
                               <span class="invalid-feedback" role="alert">
                                       <strong>{{ $errors->first('newpassword') }}</strong>
                                   </span> @endif
                               <p class="help-block text-danger"></p>
                           </div>
                       </div>
                       <button type="submit" class="btn btn-custom btn-lg" style="clear:both;display:block;margin-bottom:12px;">Change</button>           
                       {!! Form::close() !!}
                </div>
            </section>
        </div>
    </main>
</div>
@endsection
 
@section('footer')
<script>
    $('.right li').on('click', function () {
        var myId = $(this).attr('id');

        $(this).addClass('actived lane').siblings().removeClass('actived lane');

        $('.right section').hide();

        $('#' + myId + '-content').fadeIn('1000');

    });

    $('.overlay i').on('click',function(){
        $('input').trigger('click');
    });
</script>

<script>
        // PLAYER VARIABLES
        var mp3snd = "/website/sound/song-website.mp3";
        document.write('<audio autoplay="autoplay">');
        document.write('<source src="'+mp3snd+'" type="audio/mpeg">');
        document.write('<!--[if lt IE 9]>');
         document.write('<bgsound src="'+mp3snd+'" loop="1">');
        document.write('<![endif]-->');
        document.write('</audio>');
        
    </script> 
@endsection