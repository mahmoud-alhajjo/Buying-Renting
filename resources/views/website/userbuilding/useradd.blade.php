@extends('layouts.app') 
@section('title')إضافة عقار جديد
@endsection
 
@section('header') {!! Html::style('cus/addBuilding.css')!!}
@endsection
 
@section('content')

<div class="container">

    <form method="POST" enctype="multipart/form-data" action="{{url('/user/create/building')}}">
        @csrf

        <h1>Post is expected to be approved by the administration, Knowing that the property is published in a period not exceeding
            5 hours.</h1>

        <div class="contentform">

            <div class="leftcontact">

                <div class="form-group">
                    <p>Building name<span>*</span></p>
                    <span class="icon-case"><i class="fa fa-building"></i></span>
                    <input id="bu_name" type="text" class="{{ $errors->has('bu_name') ? ' is-invalid' : '' }}" name="bu_name" value="{{ old('bu_name') }}"
                        required autofocus> @if ($errors->has('bu_name'))
                    <span class="invalid-feedback" role="alert">
                            <strong style=" display: inline-flex; color: red" style=" display: inline-flex; color: red">{{ $errors->first('bu_name') }}</strong>
                        </span> @endif
                </div>

                <div class="form-group">
                    <p>price<span>*</span></p>
                    <span class="icon-case"><i class="fa fa-dollar"></i></span>
                    <input id="bu_price" type="text" class="{{ $errors->has('bu_price') ? ' is-invalid' : '' }}" name="bu_price" value="{{ old('bu_price') }}"
                        required autofocus> @if ($errors->has('bu_price'))
                    <span class="invalid-feedback" role="alert">
                            <strong style=" display: inline-flex; color: red">{{ $errors->first('bu_price') }}</strong>
                        </span> @endif
                </div>

                <div class="form-group">
                    <p>square<span>*</span></p>
                    <span class="icon-case"><i class="fa fa-h-square"></i></span>
                    <input id="bu_square" type="text" class="{{ $errors->has('bu_square') ? ' is-invalid' : '' }}" name="bu_square" value="{{ old('bu_square') }}"
                        required autofocus> @if ($errors->has('bu_square'))
                    <span class="invalid-feedback" role="alert">
                        <strong style=" display: inline-flex; color: red">{{ $errors->first('bu_square') }}</strong>
                    </span> @endif
                </div>

                <div class="form-group">
                    <p>rooms <span>*</span></p>
                    <span class="icon-case"><i class="fa fa-building-o"></i></span> {!! Form::select('rooms',rooms(),null,['class'=>'form-control','style'=>'width:75%;height:40px;','required'])
                    !!} @if ($errors->has('rooms'))
                    <span class="invalid-feedback" role="alert">
                                            <strong style=" display: inline-flex; color: red">{{ $errors->first('rooms') }}</strong>
                                        </span> @endif
                </div>

                <div class="form-group">
                    <p>type <span>*</span></p>
                    <span class="icon-case"><i class="fa fa-home"></i></span> {!! Form::select('bu_type',bu_type(),null,['class'=>'form-control','style'=>'width:75%;height:40px;','required'])
                    !!} @if ($errors->has('bu_type'))
                    <span class="invalid-feedback" role="bu_type">
                                                                                        <strong style=" display: inline-flex; color: red">{{ $errors->first('bu_type') }}</strong>
                                                                                    </span> @endif
                </div>

                <div class="form-group">
                    <p>ownership <span>*</span></p>
                    <span class="icon-case"><i class="fa fa-shopping-cart"></i></span> {!! Form::select('bu_rent',bu_rent(),null,['class'=>'form-control','style'=>'width:75%;height:40px;','required'])
                    !!} @if ($errors->has('bu_rent'))
                    <span class="invalid-feedback" role="bu_rent">
                <strong style=" display: inline-flex; color: red">{{ $errors->first('bu_rent') }}</strong>
            </span> @endif
                </div>

            </div>

            <div class="rightcontact">

                <div class="form-group">
                    <p>City <span>*</span></p>
                    <span class="icon-case"><i class="fa fa-map-marker"></i></span> {!! Form::select('bu_place',bu_place(),null,['class'=>'form-control','style'=>'width:75%;height:40px;','required'])
                    !!} @if ($errors->has('bu_place'))
                    <span class="invalid-feedback" role="bu_place">
                        <strong style=" display: inline-flex; color: red">{{ $errors->first('bu_place') }}</strong>
                    </span> @endif
                </div>

                <div class="form-group">
                    <p>Longitude</p>
                    <span class="icon-case"><i class="fa fa-thumb-tack"></i></span>
                    <input id="bu_Longitude" type="text" class="{{ $errors->has('bu_Longitude') ? ' is-invalid' : '' }}" name="bu_Longitude"
                        value="{{ old('bu_Longitude') }}"> @if ($errors->has('bu_Longitude'))
                    <span class="invalid-feedback" role="alert">
                            <strong style=" display: inline-flex; color: red">{{ $errors->first('bu_Longitude') }}</strong>
                        </span> @endif
                </div>

                <div class="form-group">
                    <p>latitude</p>
                    <span class="icon-case"><i class="fa fa-thumb-tack"></i></span>
                    <input id="bu_latitude" type="text" class="{{ $errors->has('bu_latitude') ? ' is-invalid' : '' }}" name="bu_latitude" value="{{ old('bu_latitude') }}">                    @if ($errors->has('bu_latitude'))
                    <span class="invalid-feedback" role="alert">
                            <strong style=" display: inline-flex; color: red">{{ $errors->first('bu_latitude') }}</strong>
                        </span> @endif
                </div>



                <div class="form-group">
                    <p>key words</p>
                    <span class="icon-case"><i class="fa fa-info"></i></span>
                    <input id="bu_meta" type="text" class="{{ $errors->has('bu_meta') ? ' is-invalid' : '' }}" name="bu_meta" value="{{ old('bu_meta') }}">                    @if ($errors->has('bu_meta'))
                    <span class="invalid-feedback" role="alert">
                            <strong style=" display: inline-flex; color: red">{{ $errors->first('bu_meta') }}</strong>
                        </span> @endif
                </div>

                <div class="form-group custom-file">
                    <p>Photo</p>
                    <span class="icon-case"><i class="fa fa-image" style="padding:14px 9px;cursor:pointer;"></i></span> {!!
                    Form::file('image',['class'=>'','style'=> 'display:none']) !!} @if ($errors->has('image'))
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span> @endif
                    <p class="help-block text-danger"></p>
                </div>
                <div class="form-group">
                    <p>Description <span>*</span></p>
                    <span class="icon-case"><i class="fa fa-comments-o"></i></span>
                    <textarea id="bu_large_dis" type="text" placeholder="Description about building." class="{{ $errors->has('bu_large_dis') ? ' is-invalid' : '' }}"
                        rows="14" name="bu_large_dis" value="{{ old('bu_large_dis') }}" required autofocus></textarea>                    @if ($errors->has('bu_large_dis'))
                    <span class="invalid-feedback" role="alert">
                                            <strong style=" display: inline-flex; color: red">{{ $errors->first('bu_large_dis') }}</strong>
                                        </span> @endif </textarea>
                </div>

            </div>
        </div>
        <button type="submit" class="bouton-contact">Send</button>

    </form>
</div>
@endsection
 
@section('footer')
<script>
    $('i.fa-image').on('click',function(){
        $('input').trigger('click');
    });
    $('.custom-file input[type="file"]').change(function () {
           $(this).prev('span').text($(this).val()).css('padding-left', 35);
    });

</script>

<script>
        // PLAYER VARIABLES
        var mp3snd = "/website/sound/song-website.mp3";
        document.write('<audio autoplay="autoplay">');
        document.write('<source src="'+mp3snd+'" type="audio/mpeg">');
        document.write('<!--[if lt IE 9]>');
         document.write('<bgsound src="'+mp3snd+'" loop="1">');
        document.write('<![endif]-->');
        document.write('</audio>');
        
    </script> 
@endsection