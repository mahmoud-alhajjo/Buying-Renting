@extends('layouts.app') 
@section('title') عقارات / {{$user->name}}
@endsection
 
@section('header') {!! Html::style('cus/buAll.css')!!}
@endsection
 
@section('content')
<div class="container">
    <div class="row justify-content-center">
    @include('website.page')

        <div class="col-md-9">
            <ol class="breadcrumb" style="background:#dddedd;margin-left:13px;margin-right:13px;">

                <li><a href="{{url('/')}}">Home</a></li>
                <li><a href="{{url('/ShowAllBuilding')}}">All Building</a></li>
                <li><a href="#"> {{$user->name}}</a></li>

            </ol>
    @include('website.sharefile',['bu' => $buUser])
            <div class="clearfix"></div>
            <div class="text-center">
                @if(!isset($search)) {{ $buUser->appends(Request::except('page'))->render() }} @endif
            </div>
        </div>
        <br>
        <br>

    </div>
</div>
</div>
</div>
@endsection
 
@section('footer')
@endsection