<!--
            User Profile Sidebar by @keenthemes
            A component of Metronic Theme - #1 Selling Bootstrap 3 Admin Theme in Themeforest: http://j.mp/metronictheme
            Licensed under MIT
          -->
<div class="row profile">
    <div class="col-md-3">

            @if(Auth::user())
            <div class="profile-sidebar" style="margin-bottom: 2em;">
                    <div class="profile-usertitle">
                        <div class="profile-usertitle-name" style="font-size:21px;font-weight:bold">
                           Profile 
                        </div>
                    </div>
                
                    <div class="profile-usermenu">
                        <ul class="nav">
                            <li class="{{setActive(['user','editSetting'])}}">
                                <a href="{{url('/user/editSetting')}}">
                                                    <i class="fa fa-user"></i>
                                                    My information </a>
                            </li>
                            <li class="{{setActive(['user','buildingShow'])}}">
                                <a href="{{url('/user/buildingShow')}}">
                                                    <i class="fa fa-building"></i>
                                <label class="label label-success pull-right">{{ buildingActiveCount(Auth::user()->id,1) }}</label>
                                                    My active Building </a>
                            </li>
                            <li class="{{setActive(['user','buildingShow','waiting'])}}">
                                    <a href="{{url('/user/buildingShow/waiting')}}">
                                                        <i class="fa fa-building-o"></i>
                                                        <label class="label label-warning pull-right">{{ buildingActiveCount(Auth::user()->id,0) }}</label>
                                                        My inactive Building </a>
                                </li>
                            <li class="{{setActive(['user','create','building'])}}">
                                <a href="{{url('/user/create/building')}}">
                                                    <i class="fa fa-plus"></i>
                                                    Add Building </a>
                            </li>
                        </ul>
                    </div>
                    <!-- END MENU -->
                </div>
                @endif
        @if(!Auth::user())      
        <div class="profile-sidebar" style="margin-bottom: 2em;">
            <div class="profile-usertitle">
                <div class="profile-usertitle-name" style="font-size:21px;font-weight:bold">
                    Options
                </div>
            </div>

            <div class="profile-usermenu">
                <ul class="nav">
                    <li class="{{setActive(['ShowAllBuilding'])}}">
                        <a href="{{url('ShowAllBuilding')}}">
                                            <i class="fa fa-globe"></i>
                                            All Buillding </a>
                    </li>
                    <li class="{{setActive(['forBayOrRent','0'])}}">
                        <a href="{{url('/forBayOrRent/0')}}">
                                            <i class="fa fa-shopping-cart"></i>
                                            Buying </a>
                    </li>
                    <li class="{{setActive(['forBayOrRent','1'])}}">
                        <a href="{{url('/forBayOrRent/1')}}">
                                            <i class="fa fa-dollar"></i>
                                            Rents </a>
                    </li>
                    <li class="{{setActive(['forByType','0'])}}">
                        <a href="{{url('/forByType/0')}}">
                                            <i class="fa fa-home"></i>
                                            Appartment </a>
                    </li>
                    <li class="{{setActive(['forByType','1'])}}">
                        <a href="{{url('/forByType/1')}}">
                                            <i class="fa fa-institution"></i>
                                            Vila </a>
                    </li>
                </ul>
            </div>
            <!-- END MENU -->
        </div>
@endif
        <div class="profile-sidebar">

            <div class="profile-usertitle inputsearch">

                <div class="profile-usertitle-name" style="font-size:21px;font-weight:bold">
                    Advance Search
                </div>
                {!! Form::open(['url'=>'/search','method'=>'get'])!!}
                <ul>
                    <li style="padding:10px">
                        {!! Form::text('bu_price_from',null,['class'=>'form-control','placeholder'=>'Price Buildding from'])!!}
                    </li>
                    <li style="padding:10px">
                        {!! Form::text('bu_price_to',null,['class'=>'form-control','placeholder'=>'Price Buildding to'])!!}
                    </li>
                    <li style="padding:10px">
                        {!! Form::select('bu_place',bu_place(),null,['class'=>'form-control js-example-basic-single ','placeholder'=>'Place'])!!}
                    </li>
                    <li style="padding:10px">
                        {!! Form::select("rooms",rooms(),null,['class'=>"form-control",'placeholder'=>"The number of rooms"])!!}
                    </li>
                    <li style="padding:10px">
                        {!! Form::select("bu_type",bu_type(),null,['class'=>"form-control",'placeholder'=>"Type Buildding"])!!}
                    </li>
                    <li style="padding:10px">
                        {!! Form::select("bu_rent",bu_rent(),null,['class'=>"form-control",'placeholder'=>"Ownership"])!!}
                    </li>
                    <li style="padding:10px">
                        {!! Form::text("bu_square",null,['class'=>"form-control",'placeholder'=>"Square"])!!}
                    </li>
                    <li style="padding:10px">
                        {!! Form::submit("Search",['class'=>" btn btn-custom"])!!}
                    </li>
                </ul>
                {!! Form::close()!!}
            </div>
        </div>

    </div>