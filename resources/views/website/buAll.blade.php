@extends('layouts.app') 
@section('title') all building
@endsection
 
@section('header') {!! Html::style('cus/buAll.css')!!}
@endsection
 
@section('content')
<div class="container">
    <div class="row justify-content-center">
    @include('website.page')

        <div class="col-md-9">
            <ol class="breadcrumb" style="background:#dddedd;margin-left:13px;margin-right:13px;">

                <li><a href="{{url('/')}}">Home</a></li>
                @if(isset($array)) @if(!empty($array)) @foreach($array as $key=> $value)
                <li><a href="{{url('/search?'.$key.'='.$value)}}">{{ searchNameFields()[$key] }} <i class="fa fa-angle-double-right"></i>
                    @if($key == 'bu_type') {{bu_type()[$value]}} @elseif($key == 'bu_rent') {{bu_rent()[$value]}} @elseif($key == 'bu_place')
                {{bu_place()[$value]}}@else {{$value}} @endif @endforeach @endif  @endif</a></li>
            </ol>
    @include('website.sharefile',['bu' => $buAll])
            <div class="clearfix"></div>
            <div class="text-center">
                @if(!isset($search)) {{ $buAll->appends(Request::except('page'))->render() }} @endif
            </div>
        </div>
        <br>
        <br>

{{--
<ul class="cd-items cd-container">
  <div class="section-title">
    <h2 style="font-size: 32px;">Latest Ads </h2>
  </div>
  <div class="clearfix"></div>
  @foreach(\App\Bu::where('bu_status',1)->get() as $bu)
  <li class="cd-item effect3">
    <img src="{{checkIfImageIsexist($bu->image)}}" alt="{{$bu->name}}" title="{{$bu->name}}" class="img-responsive" style="width:257.391px;height:289.563px;">
    <a href="#0" class="cd-trigger" title="{{$bu->name}}" data-id="{{$bu->id}}">Quick View</a> @endforeach
  </li>
  <!-- cd-item -->
</ul>
<!-- cd-items -->

<div class="cd-quick-view">
  <div class="cd-slider-wrapper">
    <ul class="cd-slider">
      <li class="selected"><img class="imagebox img-responsive" src="" alt="{{$bu->name}}" title="{{$bu->name}}" style="height:350px;width:300;"></li>
    </ul>
  </div>
  <!-- cd-slider-wrapper -->

  <div class="cd-item-info">
    <h2 class="titlebox"></h2>
    <p class="disbox"></p>
    <ul class="cd-item-action">
      <li><a href="" class="pricebox">Add to cart</a></li>
      <li><a href="" class="morebox">Read more</a></li>
    </ul> --}}

    <!-- Gallery Section -->


    </div>
</div>
</div>
</div>
@endsection
 
@section('footer')
<script>
    function urlHome(){
      return '{{ Request::root() }}';
    }
    function noImageUrl(){
      return '{{getSetting('no_image')}}';
    }
</script>
@endsection