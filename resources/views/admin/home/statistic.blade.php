@extends('admin.layout.layout') 
@section('title') /statistic {{$year}}
@endsection
 
@section('header')
@endsection
 
@section('content')
<section class="content-header">
    <h1>
        Statistic
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/adminpanel')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="{{url('/adminpanel/building/year/statistics')}}"> statistic</a></li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <!-- /.box-header -->
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">
                        Statistic {{$year}} </h3>
                        {!! Form::open(['url' => '/adminpanel/building/year/statistics' , 'method' => 'post','class'=>'pull-right']) !!}
                        <input name="submit" type="submit" value="Show statistic" class="btn btn-instagram" >
                        <select name="year" style="width:200px">{{$year}}
                            @for($i=2017;$i<=2055;$i++)
                        <option value="{{$i}}">{{$i}}</option>
                        @endfor
                        </select>
                        {!! Form::close() !!}
                    <div class="box-body">
                        <div id="login">
                            <div class="container">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <p class="text-center">
                                                        <strong style=" font-size:24px;color:#f32828">Statistic {{$year}}</strong>
                                                    </p>

                                                    <div class="chart">
                                                        <!-- Sales Chart Canvas -->
                                                        <canvas id="salesChart" style="height: 180px;"></canvas>
                                                    </div>
                                                    <!-- /.chart-responsive -->
                                                </div>
                                                <!-- /.col -->
                                            </div>                               
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
 
@section('footer')
<script>
    // Get context with jQuery - using jQuery's .get() method.
        var salesChartCanvas = $('#salesChart').get(0).getContext('2d');
          // This will get the first returned node in the jQuery collection.
          var salesChart       = new Chart(salesChartCanvas);
        
          var salesChartData = {
            labels  : ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December',],
            datasets: [
              {
                label               : 'Digital Goods',
                fillColor           : 'rgba(60,141,188,0.9)',
                strokeColor         : 'rgba(60,141,188,0.8)',
                pointColor          : '#3b8bba',
                pointStrokeColor    : 'rgba(60,141,188,1)',
                pointHighlightFill  : '#fff',
                pointHighlightStroke: 'rgba(60,141,188,1)',
                data                : [
                                         @foreach($new as $chart)
                                            @if(is_array($chart))
                                          {{ $chart['counting'] }},
                                          @else
                                            {{ $chart }},
                                            @endif
                                          @endforeach
                                      ]
              }
            ]
          };
</script>
@endsection