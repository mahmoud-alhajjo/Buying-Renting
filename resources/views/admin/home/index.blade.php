@extends('admin.layout.layout') 
@section('title') /dashboard
@endsection
 
@section('header')
@endsection

@section('content')
<section class="content-header">
    <h1>
      Admin Panel
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Info boxes -->
    <div class="row">
      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-aqua"><i class="fa fa-envelope-o"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">Messages</span>
            <span class="info-box-number">{{$contactusCount}}</span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
      </div>
      <!-- /.col -->
      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-green"><i class="fa fa-building"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">Activated Buildings</span>
          <span class="info-box-number">{{$buildingActive}}</span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
      </div>
      <!-- /.col -->

      <!-- fix for small devices only -->
      <div class="clearfix visible-sm-block"></div>

      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-red"><i class="fa fa-building-o"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">Inactivated Buildings</span>
            <span class="info-box-number">{{$buildingInactive}}</span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
      </div>
      <!-- /.col -->
      <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
          <span class="info-box-icon bg-yellow"><i class="ion ion-ios-people-outline"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">Members</span>
            <span class="info-box-number">{{$userCount}}</span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Monthly Recap Report</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <div class="btn-group">
                <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-wrench"></i></button>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">Action</a></li>
                  <li><a href="#">Another action</a></li>
                  <li><a href="#">Something else here</a></li>
                  <li class="divider"></li>
                  <li><a href="#">Separated link</a></li>
                </ul>
              </div>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
              <div class="col-md-12">
                <p class="text-center">
                  <strong>Sales:  Jan, 2018 - 30 Des, 2018</strong>
                </p>

                <div class="chart">
                  <!-- Sales Chart Canvas -->
                  <canvas id="salesChart" style="height: 180px;"></canvas>
                </div>
                <!-- /.chart-responsive -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
          </div>
          <!-- ./box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- Main row -->
    <div class="row">
      <!-- Left col -->
      <div class="col-md-12">
        <!-- MAP & BOX PANE -->
        <div class="box box-success">
          <div class="box-header with-border">
            <h3 class="box-title">Visitors Report</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body no-padding">
            <div class="row">
              <div class="col-md-9 col-sm-8">
                <div class="pad">
                  <!-- Map will be created here -->
                  <div id="world-map-markers" style="height: 325px;"></div>
                </div>
              </div>
              <!-- /.col -->
              <div class="col-md-3 col-sm-4">
                <div class="pad box-pane-right bg-green" style="min-height: 280px">
                  <div class="description-block margin-bottom">
                    <div class="sparkbar pad" data-color="#fff"></div>
                    <h5 class="description-header">{{$buildingActive}}</h5>
                    <span class="description-text">Activated Buildings</span>
                  </div>
                  <!-- /.description-block -->
                  <div class="description-block margin-bottom">
                        <div class="sparkbar pad" data-color="#fff"></div>
                        <h5 class="description-header">{{$buildingInactive}}</h5>
                    <span class="description-text">Inactivated Buildings</span>
                  </div>
                  <!-- /.description-block -->
                  <div class="description-block">
                        <div class="sparkbar pad" data-color="#fff"></div>
                        <h5 class="description-header">{{$buildingInactive + $buildingActive}}</h5>
                    <span class="description-text">All Buildings</span>
                  </div>
                  <!-- /.description-block -->
                </div>
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
          </div>
          <!-- /.box-body -->
        </div>
    </div>
        <!-- /.box -->
       
        <div class="col-md-8">
        <div class="row">
          <div class="col-md-6">
          <!-- TABLE: LATEST ORDERS -->
        <div class="box box-info">
                <div class="box-header with-border">
                  <h3 class="box-title">Latest Messages</h3>
      
                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <div class="table-responsive">
                    <table class="table no-margin">
                      <thead>
                      <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>View</th>
                      </tr>
                      </thead>
                      <tbody>
                          @foreach($latestContact as $lastcontactus)
                      <tr>
                        <td><a href="{{url('/adminpanel/contactus/'.$lastcontactus->id.'/edit')}}">{{$lastcontactus->contact_name}}</a></td>	
                        <td>{{$lastcontactus->contact_email}}</td>
                        @if($lastcontactus->readIt == 1)
                        <td><span class="label label-success">Yes</span></td>
                        @else
                        <td><span class="label label-danger">No</span></td>@endif
                        {{-- <td>
                          <div class="sparkbar" data-color="#00a65a" data-height="20">90,80,90,-70,61,-83,63</div>
                        </td> --}}
                      </tr>
                      @endforeach
                      </tbody>
                    </table>
                  </div>
                  <!-- /.table-responsive -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer text-center">
                        <a href="/adminpanel/contactus" class="uppercase">View All Messages</a>
                      </div>
                <!-- /.box-footer -->
              </div>
          </div>
          <!-- /.col -->

          <div class="col-md-6">
            <!-- USERS LIST -->
            <div class="box box-danger">
              <div class="box-header with-border">
                <h3 class="box-title">Latest Members</h3>

                <div class="box-tools pull-right">
                  <span class="label label-danger">8 New Members</span>
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                  </button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body no-padding">
                <ul class="users-list clearfix">
                    @foreach($latestUsers as $lastuser)
                  <li>
                    <img src="{{checkIfImageIsexistUser($lastuser->image)}}" alt="User Image" class="img-responsive" style="width:72px;height:72px">
                    <a class="users-list-name" href="{{url('/adminpanel/users/'.$lastuser->id.'/edit')}}">{{$lastuser->name}}</a>
                    <span class="users-list-date">{{$lastuser->created_at}}</span>
                  </li>
                  @endforeach
                </ul>
                <!-- /.users-list -->
              </div>
              <!-- /.box-body -->
              <div class="box-footer text-center">
                <a href="/adminpanel/users" class="uppercase">View All Users</a>
              </div>
              <!-- /.box-footer -->
            </div>
            <!--/.box -->
          </div>
          <!-- /.col -->
        </div>
    </div>
        <!-- /.row -->

       
        <!-- /.box -->
  
      <!-- /.col -->

      <div class="col-md-4">

        <!-- PRODUCT LIST -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Recently Added Buildings</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
              </button>
              <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <ul class="products-list product-list-in-box">
                @foreach($latestBuildings as $lastBilding)
              <li class="item">
                <div class="product-img">
                  <img src="{{checkIfImageIsexist($lastBilding->image)}}" alt="Product Image">
                </div>
                <div class="product-info">
                  <a href="javascript:void(0)" class="product-title">{{$lastBilding->bu_name}}
                    <span class="label label-danger pull-right">${{$lastBilding->bu_price}}</span></a>
                  <span class="product-description">
                        {{$lastBilding->bu_small_dis}}
                      </span>
                </div>
              </li>
              @endforeach
            </ul>
          </div>
          <!-- /.box-body -->
          <div class="box-footer text-center">
            <a href="adminpanel/bu" class="uppercase">View All Buildings</a>
          </div>
          <!-- /.box-footer -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </section>
@endsection

@section('footer')
<script>
// Get context with jQuery - using jQuery's .get() method.
var salesChartCanvas = $('#salesChart').get(0).getContext('2d');
  // This will get the first returned node in the jQuery collection.
  var salesChart       = new Chart(salesChartCanvas);

  var salesChartData = {
    labels  : ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
    datasets: [
      {
        label               : 'Digital Goods',
        fillColor           : 'rgba(60,141,188,0.9)',
        strokeColor         : 'rgba(60,141,188,0.8)',
        pointColor          : '#3b8bba',
        pointStrokeColor    : 'rgba(60,141,188,1)',
        pointHighlightFill  : '#fff',
        pointHighlightStroke: 'rgba(60,141,188,1)',
        data                : [ 
                                          @foreach($new as $chart)
                                            @if(is_array($chart))
                                          {{ $chart['counting'] }},
                                          @else
                                            {{$chart}},
                                            @endif
                                          @endforeach
                              ]
      }
    ]
  };

$('#world-map-markers').vectorMap({
    map              : 'world_mill_en',
    normalizeFunction: 'polynomial',
    hoverOpacity     : 0.7,
    hoverColor       : false,
    backgroundColor  : 'transparent',
    regionStyle      : {
      initial      : {
        fill            : 'rgba(210, 214, 222, 1)',
        'fill-opacity'  : 1,
        stroke          : 'none',
        'stroke-width'  : 0,
        'stroke-opacity': 1
      },
      hover        : {
        'fill-opacity': 0.7,
        cursor        : 'pointer'
      },
      selected     : {
        fill: 'yellow'
      },
      selectedHover: {}
    },
    markerStyle      : {
      initial: {
        fill  : '#00a65a',
        stroke: '#111'
      }
    },
    markers          : [
     @foreach($mapping as $map)
   { latLng: [{{$map->bu_latitude}}, {{$map->bu_longitude}}], name: '{{$map->bu_name}}' },
   @endforeach
    ]
  });
//   [
//       { latLng: [41.90, 12.45], name: 'Vatican City' },
//       { latLng: [43.73, 7.41], name: 'Monaco' },
//       { latLng: [-0.52, 166.93], name: 'Nauru' },
//       { latLng: [-8.51, 179.21], name: 'Tuvalu' },
//       { latLng: [43.93, 12.46], name: 'San Marino' },
//       { latLng: [47.14, 9.52], name: 'Liechtenstein' },
//       { latLng: [7.11, 171.06], name: 'Marshall Islands' },
//       { latLng: [17.3, -62.73], name: 'Saint Kitts and Nevis' },
//       { latLng: [3.2, 73.22], name: 'Maldives' },
//       { latLng: [35.88, 14.5], name: 'Malta' },
//       { latLng: [12.05, -61.75], name: 'Grenada' },
//       { latLng: [13.16, -61.23], name: 'Saint Vincent and the Grenadines' },
//       { latLng: [13.16, -59.55], name: 'Barbados' },
//       { latLng: [17.11, -61.85], name: 'Antigua and Barbuda' },
//       { latLng: [-4.61, 55.45], name: 'Seychelles' },
//       { latLng: [7.35, 134.46], name: 'Palau' },
//       { latLng: [42.5, 1.51], name: 'Andorra' },
//       { latLng: [14.01, -60.98], name: 'Saint Lucia' },
//       { latLng: [6.91, 158.18], name: 'Federated States of Micronesia' },
//       { latLng: [1.3, 103.8], name: 'Singapore' },
//       { latLng: [1.46, 173.03], name: 'Kiribati' },
//       { latLng: [-21.13, -175.2], name: 'Tonga' },
//       { latLng: [15.3, -61.38], name: 'Dominica' },
//       { latLng: [-20.2, 57.5], name: 'Mauritius' },
//       { latLng: [26.02, 50.55], name: 'Bahrain' },
//       { latLng: [0.33, 6.73], name: 'São Tomé and Príncipe' }
//     ]
</script>
@endsection