@extends('admin.layout.layout') 
@section('title') /العقارات
@endsection
 
@section('header') {!! Html::style("admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css")!!}
@endsection
 
@section('content')
<section class="content-header">
    <h1>
        Manege Users Opinions
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/adminpanel')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="{{url('/adminpanel/contactus')}}">Data tables Users Opinions</a></li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <!-- /.box-header -->
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Data Users Opinions</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">

                        <div class="row">
                            <div class="col-sm-12">
                                    <div class="row">
                                            <div class="col-sm-12">
                                                <table id="data" class="table table-bordered table-hover">                                    <thead>
                                                        <tr>
                                                                <th>ID</th>
                                                                <th>Name</th>
                                                                <th>Email</th> 
                                                                <th>Message</th>
                                                                <th>Status</th>
                                                                <th>Created_at</th>
                                                                <th>action</th>
                                                            </tr>
                                                        <tbody>
                                                        </tbody>
                                                 
                                                </table>
                                            
                             
                                <!-- /.box-body -->
                            </div>
                        </div>
                    </div>
                </div>
</section>
@endsection
 
@section('footer') {!! Html::script("admin/bower_components/datatables.net/js/jquery.dataTables.min.js") !!}
{!! Html::script("admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") !!}

<script type="text/javascript">

$(document).ready(function() {
    $('#data').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{ url('/adminpanel/contactus/data') }}',
        columns: [
            {data: 'id', name: 'id'},
            {data: 'contact_name', name: 'contact_name'},
            {data: 'contact_email', name: 'contact_email'},
            {data: 'contact_message', name: 'contact_message'},
            {data: 'readIt', name: 'readIt'},
            {data: 'created_at', name: 'created_at'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });
});
</script>
@endsection