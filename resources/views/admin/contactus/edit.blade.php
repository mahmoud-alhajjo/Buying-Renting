@extends('admin.layout.layout') 
@section('title') /تعديل الاستفسار {{$contact->contact_name}}
@endsection
 
@section('header')
@endsection


@section('content')
<section class="content-header">
    <h1>
        Edit Users Opinions {{$contact->contact_name}}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/adminpanel')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="{{url('/adminpanel/contactus')}}">Manege Users Opinions</a></li>
        <li class="active"><a href="{{url('/adminpanel/contactus/'.$contact->id.'/edit')}}">Edit Users Opinions : {{$contact->contact_name}}</a></li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <!-- /.box-header -->
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">
                        {{$contact->contact_name}}</h3>
                    <div class="box-body">
                        <div id="login">
                            <div class="container">
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="section-title">
                                            <h2>Edit</h2>
                                            <p>Please edit field, who want to correct.</p>
                                        </div>
                                         {!! Form::model($contact,['route'=> ['contactus.update',$contact->id],'method'=>'PATCH','files' => true]) !!}                                             @csrf
                                            <div class="row">
                                               
                                                <div class="col-md-10">
                                                    <div class="form-group">
                                                        {!! Form::text('contact_name',null,['class'=>'form-control']) !!} @if ($errors->has('contact_name'))
                                                        <span class="invalid-feedback" role="alert">
                                                                            <strong>{{ $errors->first('contact_name') }}</strong>
                                                                        </span>                                                    @endif
                                                        <p class="help-block text-danger"></p>
                                                    </div>
                                                </div>

                                               <div class="col-md-10">
                                                    <div class="form-group">
                                                        {!! Form::text('contact_email',null,['class'=>'form-control']) !!} @if ($errors->has('contact_email'))
                                                        <span class="invalid-feedback" role="alert">
                                                                                            <strong>{{ $errors->first('contact_email') }}</strong>
                                                                                        </span>                                                        @endif
                                                        <p class="help-block text-danger"></p>
                                                    </div>
                                                </div>

                                                 <div class="col-md-10">  
                                                    <div class="form-group">
                                                        {!! Form::textarea('contact_message',null,['class'=>'form-control','rows'=>'4']) !!} @if ($errors->has('contact_message'))
                                                        <span class="invalid-feedback" role="alert">
                                                                            <strong>{{ $errors->first('contact_message') }}</strong>
                                                                        </span>                                                    @endif
                                                        <p class="help-block text-danger"></p>
                                                    </div>
                                                </div>
                                            </div>

                                    </div>
                                </div>
                                <button type="submit" class="btn btn-app btn-lg" style="clear:both;display:block">
                                        <i class="fa fa-save"></i>
                                        Edit
                                    </button>
                                {!!Form::close()!!} 

                            </div>

                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
 
@section('footer')
@endsection