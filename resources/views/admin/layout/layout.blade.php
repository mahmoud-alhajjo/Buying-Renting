<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>  {{getSetting()}} @yield("title")
    </title>
    <link href="favicon.ico" rel="icon" type="image/x-icon" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    {!! Html::style("admin/bower_components/bootstrap/dist/css/bootstrap.min.css") !!}
    <!-- Font Awesome -->
    {!! Html::style("admin/bower_components/font-awesome/css/font-awesome.min.css") !!}
    <!-- Ionicons -->
    {!! Html::style("admin/bower_components/Ionicons/css/ionicons.min.css") !!}
      <!-- jvectormap -->
      {!! Html::style("admin/bower_components/jvectormap/jquery-jvectormap.css") !!}
    <!-- Theme style -->
    {!! Html::style("admin/dist/css/AdminLTE.min.css") !!}
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    {!! Html::style("admin/dist/css/skins/_all-skins.min.css") !!}
    <!-- Date Picker -->
    {!! Html::style("admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css") !!}
    <!-- Daterange picker -->
    {!! Html::style("admin/bower_components/bootstrap-daterangepicker/daterangepicker.css") !!}
    <!-- bootstrap wysihtml5 - text editor -->
    {!! Html::style("admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css") !!}
    <!--  SweetAlert for nice alert  -->
    {!! Html::style("cus/sweetalert2.min.css") !!}
    <!--  SweetAlert for searsh in options of select  -->
    {!! Html::style("cus/select2.min.css") !!} @yield("header")
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>


<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
        <header class="main-header">
            <!-- Logo -->
            <a href="{{url('/adminpanel')}}" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"><b>A</b>LT</span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg"><b>Admin</b>LTE</span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                                    <span class="sr-only">Toggle navigation</span>
                                  </a>

                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- Messages: style can be found in dropdown.less-->
                        <li class="dropdown messages-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                          <i class="fa fa-envelope-o"></i>
                                          <span class="label label-success">{{ CountUnReadMessage() }}</span>
                                        </a>
                            <ul class="dropdown-menu">
                                <li class="header">You have {{ CountUnReadMessage() }} messages</li>
                                <li>
                                    <!-- inner menu: contains the actual data -->
                                    <ul class="menu">
                                        @foreach(UnReadMessage() as $keymessage => $valueMessage)
                                        <li>
                                            <!-- start message -->
                                            <a href="{{url('/adminpanel/contactus/'.$valueMessage->id.'/edit')}}">
                                                <div class="pull-left">
                                                    <img src="{{checkIfImageIsexistUser($valueMessage->image)}}" class="img-circle" alt="User Image">
                                                </div>
                                                <h4>
                                                    {{$valueMessage->contact_name}}
                                                    <small><i class="fa fa-clock-o"></i>  {{$valueMessage->created_at}}</small>
                                                </h4>
                                                <p> {{str_limit($valueMessage->contact_message , 13)}}</p>
                                            </a>
                                        </li>
                                        <!-- end message -->
                                        @endforeach
                                    </ul>
                                </li>
                                <li class="footer"><a href={{url( "/adminpanel/contactus")}}>See All Messages</a></li>
                            </ul>
                        </li>
                        <!-- Notifications: style can be found in dropdown.less -->
                        <li class="dropdown notifications-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                          <i class="fa fa-bell-o"></i>
                                          <span class="label label-warning">{{ allBuildigAppendToStatus(0) }}</span>
                                        </a>
                            <ul class="dropdown-menu">
                                <li class="header">You have {{ allBuildigAppendToStatus(0) }} Buildings waiting actived</li>
                                <li>
                                    <!-- inner menu: contains the actual data -->
                                    <ul class="menu">
                                        @foreach(\App\Bu::where('bu_status',0)->get() as $buwaiting)
                                        <li>
                                            <button type="button" class="btn btn-defualt pull-right" style="margin-right: 10px;margin-top: 9px;padding: 1px;"><a  href="{{ url('adminpanel/change/status/'.$buwaiting->id.'/1')}}">Active</a></button>
                                            <a href="{{ url('/adminpanel/bu/'.$buwaiting->id.'/edit')}}" class="pull-left">{{$buwaiting->bu_name}}</a>
                                        </li>
                                        <div class="clearfix"></div>
                                        @endforeach
                                    </ul>
                                </li>
                                <li class="footer"><a href="/adminpanel/bu">all Buidings</a></li>
                            </ul>
                        </li>
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                          <img src="{{checkIfImageIsexistUser(Auth::user()->image)}}" class="user-image" alt="User Image">
                                          <span class="hidden-xs">{{Auth::user()->name}}</span>
                                        </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header">
                                    <img src="{{checkIfImageIsexistUser(Auth::user()->image)}}" class="img-circle" alt="User Image">

                                    <p>
                                        {{ Auth::user()->name}}
                                        <small>{{ Auth::user()->created_at}}</small>
                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="{{ url('/adminpanel/users/'.Auth::user()->id.'/edit')}}" class="btn btn-default btn-flat">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="{{ route('login') }}" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        <!-- Control Sidebar Toggle Button -->
                        <li>
                            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- Left side column. contains the logo and sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- Sidebar user panel -->
                <div class="user-panel">
                    <div class="pull-left image">
                        <img src="{{checkIfImageIsexistUser(Auth::user()->image)}}" class="img-circle" alt="User Image">
                    </div>
                    <div class="pull-left info">
                        <p>{{Auth::user()->name}}</p>
                        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                    </div>
                </div>
              
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu" data-widget="tree">
                    <li class="header">last visit :{{Auth::user()->updated_at}}</li>

                    <li class="">
                        <a href="/adminpanel"><i class="fa fa-dashboard"></i><span> Dashboard</span></a>
                    </li>

                    <li class="">
                        <a href="/adminpanel/sitesetting"><i class="fa fa-edit"></i><span> Main Settings</span></a>
                    </li>

                    <li class="">
                            <a href="{{url('/adminpanel/building/year/statistics')}}"><i class="fa fa-bar-chart"></i><span> Statistics</span></a>
                        </li>

                    <li class="treeview">
                        <a href="#">
                                            <i class="fa fa-user"></i> <span>Users</span>
                                            <span class="pull-right-container">
                                              <i class="fa fa-angle-left pull-right"></i>
                                            </span>
                                          </a>
                        <ul class="treeview-menu">
                            <li class="active"><a href="{{url('/adminpanel/users/create')}}"><i class="fa fa-user-plus"></i> Add User</a></li>
                            <li><a href="{{url('/adminpanel/users')}}"><i class="fa  fa-users"></i> All Users </a></li>
                        </ul>
                    </li>

                    <li class="treeview">
                        <a href="#">
                                                <i class="fa fa-building"></i> <span>Buildngs</span>
                                                <span class="pull-right-container">
                                                  <i class="fa fa-angle-left pull-right"></i>
                                                </span>
                                              </a>
                        <ul class="treeview-menu">
                            <li class="active"><a href="{{url('/adminpanel/bu/create')}}"><i class="fa  fa-plus-square"></i> Add Buildng</a></li>
                            <li><a href="{{url('/adminpanel/bu')}}"><i class="fa fa-bank"></i> All Buildngs </a></li>
                        </ul>
                    </li>

                    <li class="treeview">
                        <a href="#">
                                                <i class="fa fa-envelope"></i> <span>Messages</span>
                                                <span class="pull-right-container">
                                                  <i class="fa fa-angle-left pull-right"></i>
                                                </span>
                                              </a>
                        <ul class="treeview-menu">
                            <li><a href="{{url('/adminpanel/contactus')}}"><i class="fa fa-commenting-o"></i> All Users Opinions</a></li>
                        </ul>
                    </li>

                </ul>
            </section>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            @yield('content')
        </div>

        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>HooT</b> 1.3.0
            </div>
            <strong>Copyright &copy; 2018 <a href="#Hoot">HOOT Studio</a>.</strong> All rights reserved.
        </footer>

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Create the tabs -->
            <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
                    <li><a href="#control-sidebar-theme-demo-options-tab" data-toggle="tab" aria-expanded="true"><i class="fa fa-wrench"></i></a></li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <!-- Home tab content -->
                <div id="control-sidebar-theme-demo-options-tab" class="tab-pane active"><div><h4 class="control-sidebar-heading">Layout Options</h4><div class="form-group"><label class="control-sidebar-subheading"><input type="checkbox" data-layout="fixed" class="pull-right"> Fixed layout</label><p>Activate the fixed layout. You can't use fixed and boxed layouts together</p></div><div class="form-group"><label class="control-sidebar-subheading"><input type="checkbox" data-layout="layout-boxed" class="pull-right"> Boxed Layout</label><p>Activate the boxed layout</p></div><div class="form-group"><label class="control-sidebar-subheading"><input type="checkbox" data-layout="sidebar-collapse" class="pull-right"> Toggle Sidebar</label><p>Toggle the left sidebar's state (open or collapse)</p></div><div class="form-group"><label class="control-sidebar-subheading"><input type="checkbox" data-enable="expandOnHover" class="pull-right"> Sidebar Expand on Hover</label><p>Let the sidebar mini expand on hover</p></div><div class="form-group"><label class="control-sidebar-subheading"><input type="checkbox" data-controlsidebar="control-sidebar-open" class="pull-right"> Toggle Right Sidebar Slide</label><p>Toggle between slide over content and push content effects</p></div><div class="form-group"><label class="control-sidebar-subheading"><input type="checkbox" data-sidebarskin="toggle" class="pull-right"> Toggle Right Sidebar Skin</label><p>Toggle between dark and light skins for the right sidebar</p></div><h4 class="control-sidebar-heading">Skins</h4><ul class="list-unstyled clearfix"><li style="float:left; width: 33.33333%; padding: 5px;"><a href="javascript:void(0)" data-skin="skin-blue" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover"><div><span style="display:block; width: 20%; float: left; height: 7px; background: #367fa9"></span><span class="bg-light-blue" style="display:block; width: 80%; float: left; height: 7px;"></span></div><div><span style="display:block; width: 20%; float: left; height: 20px; background: #222d32"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div></a><p class="text-center no-margin">Blue</p></li><li style="float:left; width: 33.33333%; padding: 5px;"><a href="javascript:void(0)" data-skin="skin-black" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover"><div style="box-shadow: 0 0 2px rgba(0,0,0,0.1)" class="clearfix"><span style="display:block; width: 20%; float: left; height: 7px; background: #fefefe"></span><span style="display:block; width: 80%; float: left; height: 7px; background: #fefefe"></span></div><div><span style="display:block; width: 20%; float: left; height: 20px; background: #222"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div></a><p class="text-center no-margin">Black</p></li><li style="float:left; width: 33.33333%; padding: 5px;"><a href="javascript:void(0)" data-skin="skin-purple" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover"><div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-purple-active"></span><span class="bg-purple" style="display:block; width: 80%; float: left; height: 7px;"></span></div><div><span style="display:block; width: 20%; float: left; height: 20px; background: #222d32"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div></a><p class="text-center no-margin">Purple</p></li><li style="float:left; width: 33.33333%; padding: 5px;"><a href="javascript:void(0)" data-skin="skin-green" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover"><div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-green-active"></span><span class="bg-green" style="display:block; width: 80%; float: left; height: 7px;"></span></div><div><span style="display:block; width: 20%; float: left; height: 20px; background: #222d32"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div></a><p class="text-center no-margin">Green</p></li><li style="float:left; width: 33.33333%; padding: 5px;"><a href="javascript:void(0)" data-skin="skin-red" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover"><div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-red-active"></span><span class="bg-red" style="display:block; width: 80%; float: left; height: 7px;"></span></div><div><span style="display:block; width: 20%; float: left; height: 20px; background: #222d32"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div></a><p class="text-center no-margin">Red</p></li><li style="float:left; width: 33.33333%; padding: 5px;"><a href="javascript:void(0)" data-skin="skin-yellow" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover"><div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-yellow-active"></span><span class="bg-yellow" style="display:block; width: 80%; float: left; height: 7px;"></span></div><div><span style="display:block; width: 20%; float: left; height: 20px; background: #222d32"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div></a><p class="text-center no-margin">Yellow</p></li><li style="float:left; width: 33.33333%; padding: 5px;"><a href="javascript:void(0)" data-skin="skin-blue-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover"><div><span style="display:block; width: 20%; float: left; height: 7px; background: #367fa9"></span><span class="bg-light-blue" style="display:block; width: 80%; float: left; height: 7px;"></span></div><div><span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div></a><p class="text-center no-margin" style="font-size: 12px">Blue Light</p></li><li style="float:left; width: 33.33333%; padding: 5px;"><a href="javascript:void(0)" data-skin="skin-black-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover"><div style="box-shadow: 0 0 2px rgba(0,0,0,0.1)" class="clearfix"><span style="display:block; width: 20%; float: left; height: 7px; background: #fefefe"></span><span style="display:block; width: 80%; float: left; height: 7px; background: #fefefe"></span></div><div><span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div></a><p class="text-center no-margin" style="font-size: 12px">Black Light</p></li><li style="float:left; width: 33.33333%; padding: 5px;"><a href="javascript:void(0)" data-skin="skin-purple-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover"><div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-purple-active"></span><span class="bg-purple" style="display:block; width: 80%; float: left; height: 7px;"></span></div><div><span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div></a><p class="text-center no-margin" style="font-size: 12px">Purple Light</p></li><li style="float:left; width: 33.33333%; padding: 5px;"><a href="javascript:void(0)" data-skin="skin-green-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover"><div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-green-active"></span><span class="bg-green" style="display:block; width: 80%; float: left; height: 7px;"></span></div><div><span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div></a><p class="text-center no-margin" style="font-size: 12px">Green Light</p></li><li style="float:left; width: 33.33333%; padding: 5px;"><a href="javascript:void(0)" data-skin="skin-red-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover"><div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-red-active"></span><span class="bg-red" style="display:block; width: 80%; float: left; height: 7px;"></span></div><div><span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div></a><p class="text-center no-margin" style="font-size: 12px">Red Light</p></li><li style="float:left; width: 33.33333%; padding: 5px;"><a href="javascript:void(0)" data-skin="skin-yellow-light" style="display: block; box-shadow: 0 0 3px rgba(0,0,0,0.4)" class="clearfix full-opacity-hover"><div><span style="display:block; width: 20%; float: left; height: 7px;" class="bg-yellow-active"></span><span class="bg-yellow" style="display:block; width: 80%; float: left; height: 7px;"></span></div><div><span style="display:block; width: 20%; float: left; height: 20px; background: #f9fafc"></span><span style="display:block; width: 80%; float: left; height: 20px; background: #f4f5f7"></span></div></a><p class="text-center no-margin" style="font-size: 12px">Yellow Light</p></li></ul></div></div>
                
                <!-- /.tab-pane -->
                <!-- Stats tab content -->
                <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
                <!-- /.tab-pane -->
                <!-- Settings tab content -->
               
                <!-- /.tab-pane -->
            </div>
        </aside>
        <!-- /.control-sidebar -->
        <!-- Add the sidebar's background. This div must be placed
                                       immediately after the control sidebar -->
        <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->
    <!-- jQuery 3 -->
    {!! Html::script("admin/bower_components/jquery/dist/jquery.min.js") !!}
    <!-- jQuery UI 1.11.4 -->
    {!! Html::script("admin/bower_components/jquery-ui/jquery-ui.min.js") !!}
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.7 -->
    {!! Html::script("admin/bower_components/bootstrap/dist/js/bootstrap.min.js") !!}
      <!-- FastClick -->
      {!! Html::script("admin/bower_components/fastclick/lib/fastclick.js") !!}
      <!-- AdminLTE App -->
    {!! Html::script("admin/dist/js/adminlte.min.js") !!}
    <!-- Sparkline -->
    {!! Html::script("admin/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js") !!}
    <!-- jvectormap -->
    {!! Html::script("admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js") !!} {!! Html::script("admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js")
    !!}
    <!-- Slimscroll -->
    {!! Html::script("admin/bower_components/jquery-slimscroll/jquery.slimscroll.min.js") !!}
    <!-- ChartJS -->
    {!! Html::script("admin/bower_components/chart.js/Chart.js") !!}
     <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
     {!! Html::script("admin/dist/js/pages/dashboard2.js") !!}
     <!-- AdminLTE for demo purposes -->
    {!! Html::script("admin/dist/js/demo.js") !!}
    <!-- jQuery Knob Chart -->
    {!! Html::script("admin/bower_components/jquery-knob/dist/jquery.knob.min.js") !!}
    <!-- daterangepicker -->
    {!! Html::script("admin/bower_components/moment/min/moment.min.js") !!} {!! Html::script("admin/bower_components/bootstrap-daterangepicker/daterangepicker.js")!!}
    <!-- datepicker -->
    {!! Html::script("admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js") !!}
    <!-- Bootstrap WYSIHTML5 -->
    {!! Html::script("admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js") !!}

    <!--  SweetAlert for nice alert  -->
    {!! Html::script("cus/sweetalert2.min.js") !!} {!! Html::script("cus/select2.min.js") !!}
    <script type="text/javascript">
        $('.js-example-basic-single').select2();
    </script>
    @yield("footer")
    @include('admin.layout.flash_massage')
</body>

</html>