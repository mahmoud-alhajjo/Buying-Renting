@extends('admin.layout.layout') 
@section('title') /تعديل العضو {{$user->name}}
@endsection
 
@section('header')
@endsection


@section('content')
<section class="content-header">
    <h1>
        Edit User: {{$user->name}}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/adminpanel')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="{{url('/adminpanel/users')}}">Manege Users</a></li>
        <li class="active"><a href="{{url('/adminpanel/users/'.$user->id.'/edit')}}">Edit User : {{$user->name}}</a></li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12 col-md-9">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#settings" data-toggle="tab" aria-expanded="false">Settings</a></li>
                    <li class=""><a href="#passwordwwww" data-toggle="tab" aria-expanded="false">Change Password</a></li>
                    <li class=""><a href="#activity" data-toggle="tab" aria-expanded="true">Activity Building</a></li>
                    <li class=""><a href="#timeline" data-toggle="tab" aria-expanded="false">Inactivity Building</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="settings">

                        <div class="section-title" style="margin-left: 16px;">
                            <h2>{{$user->name}}</h2>
                            <p class="help-block">Please edit field, who want to correct.</p>
                        </div>
                        {!! Form::model($user,['route'=> ['users.update',$user->id],'method'=>'PATCH','files' => true]) !!}
                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::text('name',null,['class'=>'form-control']) !!} @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                                                                                    <strong>{{ $errors->first('name') }}</strong>
                                                                                                </span>                                @endif
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                {!! Form::text('email',null,['class'=>'form-control']) !!} @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                                                                                    <strong>{{ $errors->first('email') }}</strong>
                                                                                                </span>                                @endif
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>

                        <div class="col-md-6">
                            @if($user->image != "")
                            <img src="{{Request::root().'/website/user_image/'.$user->image}}" class="img-responsive" height="100" width="100" alt="don't care">                            @endif
                            <div class="clearfix"></div>
                            <br>
                            <div class="form-group">
                                {!! Form::file('image',null,['class'=>'form-control']) !!} @if ($errors->has('image'))
                                <span class="invalid-feedback" role="alert">
                                                                                                                        <strong>{{ $errors->first('image') }}</strong>
                                                                                                                    </span>                                @endif
                                <p class="help-block text-danger"></p>
                                <p class="help-block" style="margin-bottom:20px">insert new personal photo.</p>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group" style="margin-bottom: 28px;margin-top: 10px;">
                                {!! Form::select('admin',['0'=>'User',"1"=>"Manager"]) !!} @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                                                                                    <strong>{{ $errors->first('name') }}</strong>
                                                                                                </span>                                @endif
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary" style="clear:both;display:block;margin-bottom:12px;margin-left: 16px;">
                                                                    <i class="fa fa-edit"></i>Edit</button>                        {!! Form::close() !!}
                    </div>

                    <div class="tab-pane" id="passwordwwww">
                        <div class="section-title" style="margin-left: 16px;">
                            <h2>{{$user->name}}</h2>
                            <p class="help-block">Create a new passwrd</p>
                        </div>

                        {!! Form::open(['url'=>'/adminpanel/user/changePassword/','method'=>"post",'style' => 'height:111px'])!!}
                        <input type="hidden" value='{{$user->id}}' name="user_id">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input id="password" type="password" placeholder="Write a new password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                    name="password" required> @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                                                                            <strong>{{ $errors->first('password') }}</strong>
                                                                                        </span>                                @endif
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary" style="clear:both;display:block;margin-bottom:12px;margin-left: 16px;">
                                                                                <i class="fa fa-edit"></i>Change</button>                        {!!Form::close()!!}
                    </div>

                    <div class="tab-pane" id="activity">
                        <table class="table table-bordered">
                            <tr>
                                <td>Name Building</td>
                                <td>Created_at</td>
                                <td>Ownership</td>
                                <td>Price</td>
                                <td>Type</td>
                                <td>Place</td>
                                <td>Notenable</td>
                                <td>Delete</td>
                            </tr>
                            @foreach($buenable as $enable)
                            <tr>
                                <td><a href="{{ url('/adminpanel/bu/'.$enable->id.'/edit')}}">{{ $enable->bu_name }}</a></td>
                                <td>{{ $enable->created_at}}</td>
                                <td>{{ bu_rent()[$enable->bu_rent]}}</td>
                                <td>{{ $enable->bu_price}}</td>
                                <td>{{ bu_type()[$enable->bu_type]}}</td>
                                <td>{{ bu_place()[$enable->bu_place]}}</td>
                                <td><a href="{{url('adminpanel/change/status/'.$enable->id.'/0')}}" class="btn btn-xs btn-warning"><i class="glyphicon glyphicon-remove"></i>Inactiveted</a></td>
                                <td><a href="{{url('adminpanel/bu/'.$enable->id.'/delete')}}" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-remove"></i>Delete</a></td>
                            </tr>
                            @endforeach
                        </table>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="timeline">
                        <table class="table table-bordered">
                            <tr>
                                <td>Name Building</td>
                                <td>Created_at</td>
                                <td>Ownership</td>
                                <td>Price</td>
                                <td>Type</td>
                                <td>Place</td>
                                <td>Enable</td>
                                <td>Delete</td>
                            </tr>
                            @foreach($buWaiting as $waiting)
                            <tr>
                                <td><a href="{{ url('/adminpanel/bu/'.$waiting->id.'/edit')}}">{{ $waiting->bu_name }}</a></td>
                                <td>{{ $waiting->created_at}}</td>
                                <td>{{ bu_rent()[$waiting->bu_rent]}}</td>
                                <td>{{ $waiting->bu_price}}</td>
                                <td>{{ bu_type()[$waiting->bu_type]}}</td>
                                <td>{{ bu_place()[$waiting->bu_place]}}</td>
                                <td><a href="{{ url('adminpanel/change/status/'.$waiting->id.'/1')}}" class="btn btn-xs btn-warning"><i class="glyphicon glyphicon-remove"></i>Activeted</a></td>
                                <td><a href="{{url('adminpanel/bu/'.$waiting->id.'/delete')}}" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-remove"></i>Delete</a></td>

                            </tr>
                            @endforeach
                        </table>
                    </div>
                    <!-- /.tab-pane -->

                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
            <!-- /.nav-tabs-custom -->
        </div>
    </div>
</section>
@endsection
 
@section('footer')
@endsection