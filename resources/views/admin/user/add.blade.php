@extends('admin.layout.layout') 
@section('title') /add user
@endsection
 
@section('header')
@endsection
 
@section('content')
<section class="content-header">
    <h1>
        Add User
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/adminpanel')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class=""><a href="{{url('/adminpanel/users')}}">Manege Users</a></li>
        <li class="active"><a href="{{url('/adminpanel/users/create')}}">Add User</a></li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <!-- /.box-header -->
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Add New User</h3>
                    <div class="box-body">
                        <div id="login">
                            <div class="container">
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="section-title">
                                            <h2>Register</h2>
                                            <p class="help-block">Please fill out the form below to be member.</p>
                                        </div>
                                        <form method="POST" enctype="multipart/form-data" role="form" action="{{url('/adminpanel/users')}}">
                                            @csrf
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <input id="name" type="text" placeholder="Name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name"
                                                            value="{{ old('name') }}" required autofocus>                                                        @if ($errors->has('name'))
                                                        <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $errors->first('name') }}</strong>
                                                                    </span>                                                        @endif
                                                        <p class="help-block text-danger"></p>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <input id="email" type="email" placeholder="E-Mail Address" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                                            name="email" value="{{ old('email') }}" required>                                                        @if ($errors->has('email'))
                                                        <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $errors->first('email') }}</strong>
                                                                    </span>                                                        @endif
                                                        <p class="help-block text-danger"></p>
                                                    </div>
                                                </div>



                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <input id="password" type="password" placeholder="Password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                                            name="password" required>                                                        @if ($errors->has('password'))
                                                        <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $errors->first('password') }}</strong>
                                                                    </span>                                                        @endif
                                                        <p class="help-block text-danger"></p>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <input id="password-confirm" type="password" placeholder="Confirm Password" class="form-control" name="password_confirmation"
                                                            required>
                                                        <p class="help-block text-danger"></p>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        {!! Form::file('image',null,['class'=>'form-control']) !!} @if ($errors->has('image'))
                                                        <span class="invalid-feedback" role="alert">
                                                                                                <strong>{{ $errors->first('image') }}</strong>
                                                                                            </span>                                                        @endif
                                                        <p class="help-block text-danger"></p>
                                                        <p class="help-block">insert personal photo.</p>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <button type="submit" class="btn btn-primary" style="margin: 0;margin-top: 40px;">
                                                        <i class="fa fa-save"></i>
                                                        Register
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
 
@section('footer')
@endsection