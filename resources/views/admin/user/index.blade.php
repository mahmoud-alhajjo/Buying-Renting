@extends('admin.layout.layout') 
@section('title') /الأعضاء
@endsection
 
@section('header') {!! Html::style("admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css")!!}
@endsection
 
@section('content')
<section class="content-header">
    <h1>
        Manege Users
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/adminpanel')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="{{url('/adminpanel/users')}}">Data tables Users</a></li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <!-- /.box-header -->
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Data Table Users</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                  
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="data" class="table table-bordered table-hover">                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Created_at </th>
                                            <th>permission</th>
                                            <th>Control</th>
                                        </tr>
                                   
                                </table>
                            
                                <!-- /.box-body -->
                            </div>
                        </div>
                    </div>
                </div>
</section>
@endsection
 
@section('footer') {!! Html::script("admin/bower_components/datatables.net/js/jquery.dataTables.min.js") !!}
{!! Html::script("admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") !!}


<script type="text/javascript">

    $(document).ready(function() {
        $('#data').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ url('/adminpanel/users/data') }}',
            columns: [
                {data: 'id', name: 'id'},
                {data: 'name', name: 'name'},
                {data: 'email', name: 'email'},
                {data: 'created_at', name: 'created_at'},
                {data: 'admin', name: 'admin'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });
    });
    </script>
@endsection