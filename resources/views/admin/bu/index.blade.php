@extends('admin.layout.layout') 
@section('title') /العقارات
@endsection
 
@section('header') {!! Html::style("admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css")!!}
@endsection
 
@section('content')
<section class="content-header">
    <h1>
        Manege Buildings
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/adminpanel')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="{{url('/adminpanel/bu')}}">Data tables Building</a></li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <!-- /.box-header -->
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Data Table Building</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">

                        <div class="row">
                            <div class="col-sm-12">
                                    <div class="row">
                                            <div class="col-sm-12">
                                                <table id="data" class="table table-bordered table-hover">                                    <thead>
                                                        <tr>
                                                                <th>رقم العقار</th>
                                                                <th>اسم العقار</th>
                                                                <th>عدد الغرف </th> 
                                                                 <th>السعر</th>
                                                                  <th>أضيف في</th>
                                                                  <th>النوع</th>
                                                                  <th>الحالة</th>
                                                                  <th>المنطقة</th>
                                                                  <th> action</th>
                                                            </tr>
                                                        <tbody>
                                                        </tbody>
                                                </table>
                                            
                             
                                <!-- /.box-body -->
                            </div>
                        </div>
                    </div>
                </div>
</section>
@endsection
 
@section('footer') {!! Html::script("admin/bower_components/datatables.net/js/jquery.dataTables.min.js") !!}
{!! Html::script("admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js") !!}

<script type="text/javascript">

$(document).ready(function() {
    $('#data').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{ url('/adminpanel/bu/data') }}{{$id}}',
        columns: [
            {data: 'id', name: 'id'},
            {data: 'bu_name', name: 'bu_name'},
            {data: 'rooms', name: 'rooms'},
            {data: 'bu_price', name: 'bu_price'},
            {data: 'created_at', name: 'created_at'},
            {data: 'bu_type', name: 'bu_type'},
            {data: 'bu_status', name: 'bu_status'},
            {data: 'bu_place', name: 'bu_place'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });
});
</script>
@endsection