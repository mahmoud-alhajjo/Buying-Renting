@extends('admin.layout.layout') 
@section('title') /أضف عقار
@endsection
 
@section('header')
@endsection
 
@section('content')
<section class="content-header">
    <h1>
        Add Building
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/adminpanel')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class=""><a href="{{url('/adminpanel/bu')}}">Manege Buildings</a></li>
        <li class="active"><a href="{{url('/adminpanel/bu/create')}}">Add Building</a></li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <!-- /.box-header -->
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Add New Building</h3>
                    <div class="box-body">
                        <div id="login">
                            <div class="container">
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="section-title">
                                            <h2>Register</h2>
                                            <p>Please fill out the form below to adding Building.</p>
                                        </div>
                                        <form method="POST" enctype="multipart/form-data" role="form" action="{{url('/adminpanel/bu')}}">
                                            @csrf
                                            <div class="row">
                                                <div class="col-md-10">
                                                    <div class="form-group">
                                                        <input id="bu_name" type="text" placeholder="Name Building" class="form-control{{ $errors->has('bu_name') ? ' is-invalid' : '' }}"
                                                            name="bu_name" value="{{ old('bu_name') }}" required autofocus>                                                        @if ($errors->has('bu_name'))
                                                        <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $errors->first('bu_name') }}</strong>
                                                                    </span>                                                        @endif
                                                        <p class="help-block text-danger"></p>
                                                    </div>
                                                </div>

                                                <div class="col-md-10">
                                                    <div class="form-group">
                                                        {!! Form::select('bu_place',bu_place(),null,['class'=>'form-control js-example-basic-single']) !!} @if ($errors->has('bu_place'))
                                                        <span class="invalid-feedback" role="alert">
                                                                                            <strong>{{ $errors->first('bu_place') }}</strong>
                                                                                        </span>                                                        @endif
                                                        <p class="help-block text-danger"></p>
                                                    </div>
                                                </div>

                                                <div class="col-md-10">
                                                        <div class="form-group">
                                                            {!! Form::file('image',null,['class'=>'form-control']) !!} @if ($errors->has('image'))
                                                            <span class="invalid-feedback" role="alert">
                                                                                                <strong>{{ $errors->first('image') }}</strong>
                                                                                            </span>                                                        @endif
                                                            <p class="help-block text-danger"></p>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-10">
                                                            <div class="form-group">
                                                                {!! Form::select('rooms',rooms(),null,['class'=>'form-control']) !!} @if ($errors->has('rooms'))
                                                                <span class="invalid-feedback" role="alert">
                                                                                                    <strong>{{ $errors->first('rooms') }}</strong>
                                                                                                </span>                                                        @endif
                                                                <p class="help-block text-danger"></p>
                                                            </div>
                                                        </div>    

                                                <div class="col-md-10">
                                                    <div class="form-group">
                                                        <input id="bu_price" type="text" placeholder="Price" class="form-control{{ $errors->has('bu_price') ? ' is-invalid' : '' }}"
                                                            name="bu_price" value="{{ old('bu_price') }}" required autofocus>                                                        @if ($errors->has('bu_price'))
                                                        <span class="invalid-feedback" role="alert">
                                                                                <strong>{{ $errors->first('bu_price') }}</strong>
                                                                            </span>                                                        @endif
                                                        <p class="help-block text-danger"></p>
                                                    </div>
                                                </div>

                                                <div class="col-md-10">
                                                    <div class="form-group">
                                                        {!! Form::select('bu_rent',bu_rent(),null,['class'=>'form-control']) !!} @if ($errors->has('bu_rent'))
                                                        <span class="invalid-feedback" role="alert">
                                                                                            <strong>{{ $errors->first('bu_rent') }}</strong>
                                                                                        </span>                                                        @endif
                                                        <p class="help-block text-danger"></p>
                                                    </div>
                                                </div>

                                                <div class="col-md-10">
                                                    <div class="form-group">
                                                        {!! Form::select('bu_status',bu_status(),null,['class'=>'form-control']) !!} @if ($errors->has('bu_status'))
                                                        <span class="invalid-feedback" role="alert">
                                                                                            <strong>{{ $errors->first('bu_status') }}</strong>
                                                                                        </span>                                                        @endif
                                                        <p class="help-block text-danger"></p>
                                                    </div>
                                                </div>

                                              

                                                <div class="col-md-10">
                                                    <div class="form-group">
                                                        {!! Form::select('bu_type',bu_type(),null,['class'=>'form-control']) !!} @if ($errors->has('bu_type'))
                                                        <span class="invalid-feedback" role="alert">
                                                                                                <strong>{{ $errors->first('bu_type') }}</strong>
                                                                                            </span>                                                        @endif
                                                        <p class="help-block text-danger"></p>
                                                    </div>
                                                </div>

                                                <div class="col-md-10">
                                                    <div class="form-group">
                                                        <input id="bu_meta" type="text" placeholder="KeyWords" class="form-control{{ $errors->has('bu_meta') ? ' is-invalid' : '' }}"
                                                            name="bu_meta" value="{{ old('bu_meta') }}" required autofocus>                                                        @if ($errors->has('bu_meta'))
                                                        <span class="invalid-feedback" role="alert">
                                                                                            <strong>{{ $errors->first('bu_meta') }}</strong>
                                                                                        </span>                                                        @endif
                                                        <p class="help-block text-danger"></p>
                                                    </div>
                                                </div>

                                                <div class="col-md-10">
                                                    <div class="form-group">
                                                        <input id="bu_square" type="text" placeholder="Square" class="form-control{{ $errors->has('bu_square') ? ' is-invalid' : '' }}"
                                                            name="bu_square" value="{{ old('bu_square') }}" required autofocus>                                                        @if ($errors->has('bu_square'))
                                                        <span class="invalid-feedback" role="alert">
                                                                                <strong>{{ $errors->first('bu_square') }}</strong>
                                                                            </span>                                                        @endif
                                                        <p class="help-block text-danger"></p>
                                                    </div>
                                                </div>


                                                <div class="col-md-10">
                                                    <div class="form-group">
                                                        <input id="bu_longitude" type="text" placeholder="Longitude" class="form-control{{ $errors->has('	bu_longitude') ? ' is-invalid' : '' }}"
                                                            name="bu_longitude" value="{{ old('bu_longitude') }}" required autofocus>                                                        @if ($errors->has('bu_longitude'))
                                                        <span class="invalid-feedback" role="alert">
                                                                                <strong>{{ $errors->first('bu_longitude') }}</strong>
                                                                            </span>                                                        @endif
                                                        <p class="help-block text-danger"></p>
                                                    </div>
                                                </div>


                                                <div class="col-md-10">
                                                    <div class="form-group">
                                                        <input id="bu_latitude" type="text" placeholder="Latitude" class="form-control{{ $errors->has('bu_latitude') ? ' is-invalid' : '' }}"
                                                            name="bu_latitude" value="{{ old('bu_latitude') }}" required autofocus>                                                        @if ($errors->has('bu_latitude'))
                                                        <span class="invalid-feedback" role="alert">
                                                                                <strong>{{ $errors->first('bu_latitude') }}</strong>
                                                                            </span>                                                        @endif
                                                        <p class="help-block text-danger"></p>
                                                    </div>
                                                </div>


                                                <div class="col-md-10">
                                                    <div class="form-group">
                                                        <textarea id="bu_small_dis" type="text" placeholder="Description of the property for search engines" class="form-control{{ $errors->has('bu_small_dis') ? ' is-invalid' : '' }}"
                                                            name="bu_small_dis" value="{{ old('bu_small_dis') }}" required autofocus></textarea>                                                        @if ($errors->has('bu_small_dis'))
                                                        <span class="invalid-feedback" role="alert">
                                                                                <strong>{{ $errors->first('bu_small_dis') }}</strong>
                                                                            </span>                                                        @endif
                                                        <p class="help-block text-danger"></p>
                                                    </div>
                                                </div>


                                                <div class="col-md-10">
                                                    <div class="form-group">
                                                        <textarea id="bu_large_dis" type="text" placeholder="Lengthy Description" class="form-control{{ $errors->has('bu_large_dis') ? ' is-invalid' : '' }}"
                                                            name="bu_large_dis" value="{{ old('bu_large_dis') }}" required autofocus></textarea>                                                        @if ($errors->has('bu_large_dis'))
                                                        <span class="invalid-feedback" role="alert">
                                                                                <strong>{{ $errors->first('bu_large_dis') }}</strong>
                                                                            </span>                                                        @endif
                                                        <p class="help-block text-danger"></p>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-app btn-lg" style="clear:both;display:block">
                                        <i class="fa fa-save"></i>
                                        Register
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
 
@section('footer')
@endsection