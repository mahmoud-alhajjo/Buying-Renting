@extends('admin.layout.layout') 
@section('title') / edit {{$bu->bu_name}}
@endsection
 
@section('header')
@endsection


@section('content')
<section class="content-header">
    <h1>
        Edit Building: {{$bu->bu_name}}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/adminpanel')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="{{url('/adminpanel/bu')}}">Manege Buildings</a></li>
        <li class="active"><a href="{{url('/adminpanel/bu/'.$bu->id.'/edit')}}">Edit Building : {{$bu->bu_name}}</a></li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <!-- /.box-header -->
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">
                        {{$bu->bu_name}}</h3>
                    <div class="box-body">
                        <div id="login">
                            <div class="container">
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="section-title">
                                            @if($user == "Visitor")
                                            <h3>This building entered from normal Visitor</h3>
                                            @else
                                            <h3>information about user who entered this building:</h3>
                                        <h3>User Name: </h3><span class="help-block">{{ $user->name}}</span>
                                        <h3>Email: </h3><span class="help-block">{{ $user->email}}</span>
                                        <h3>Promision: </h3>
                                        @if($user->admin == 0)
                                        <span class="help-block">User</span>
                                        @else
                                        <span class="help-block">Maneger</span>
                                        @endif
                                        <h3>More about User: </h3><span><a href="{{ url('/adminpanel/users/'.$user->id.'/edit')}}">More details</a></span>
                                        </div>
                                        @endif
                                        <hr>
                                        <hr>
                                        {!! Form::model($bu,['route'=> ['bu.update',$bu->id],'method'=>'PATCH','files' => true]) !!}                                             @csrf
                                            <div class="row">
                                               
                                                <div class="col-md-10">
                                                    <div class="form-group">
                                                            <label for="">Name Building : </label>
                                                        {!! Form::text('bu_name',null,['class'=>'form-control']) !!} @if ($errors->has('bu_name'))
                                                        <span class="invalid-feedback" role="alert">
                                                                            <strong>{{ $errors->first('bu_name') }}</strong>
                                                                        </span>                                                    @endif
                                                        <p class="help-block text-danger"></p>
                                                    </div>
                                                </div>

                                                <div class="col-md-10">
                                                    <div class="form-group">
                                                            <label for="">Place : </label>
                                                        {!! Form::select('bu_place',bu_place(),null,['class'=>'form-control js-example-basic-single']) !!} @if ($errors->has('bu_place'))
                                                        <span class="invalid-feedback" role="alert">
                                                                                            <strong>{{ $errors->first('bu_place') }}</strong>
                                                                                        </span>                                                        @endif
                                                        <p class="help-block text-danger"></p>
                                                    </div>
                                                </div>

                                                <div class="col-md-10">
                                                        <label for="">Image : </label>
                                                    @if($bu->image != "")
                                                        <img src="{{Request::root().'/website/bu_image/'.$bu->image}}" width="100" alt="don't care">
                                                       @endif
                                                       <div class="clearfix"></div>
                                                       <br>
                                                        <div class="form-group">
                                                            {!! Form::file('image',null,['class'=>'form-control']) !!} @if ($errors->has('image'))
                                                            <span class="invalid-feedback" role="alert">
                                                                                                <strong>{{ $errors->first('image') }}</strong>
                                                                                            </span>                                                        @endif
                                                            <p class="help-block text-danger"></p>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-10">
                                                            <div class="form-group">
                                                                    <label for="">Number of rooms : </label>
                                                                {!! Form::select('rooms',rooms(),null,['class'=>'form-control']) !!} @if ($errors->has('rooms'))
                                                                <span class="invalid-feedback" role="alert">
                                                                                                    <strong>{{ $errors->first('rooms') }}</strong>
                                                                                                </span>                                                        @endif
                                                                <p class="help-block text-danger"></p>
                                                            </div>
                                                        </div>  

                                              
                                                <div class="col-md-10">
                                                    <div class="form-group">
                                                            <label for="">Price : </label>
                                                        {!! Form::text('bu_price',null,['class'=>'form-control']) !!} @if ($errors->has('bu_price'))
                                                        <span class="invalid-feedback" role="alert">
                                                                            <strong>{{ $errors->first('bu_price') }}</strong>
                                                                        </span>                                                    @endif
                                                        <p class="help-block text-danger"></p>
                                                    </div>
                                                </div>

                           

                                               <div class="col-md-10">
                                                <div class="form-group">
                                                        <label for="">Ownership : </label>
                                                    {!! Form::select('bu_rent',['0'=>'sale',"1"=>"rent"]) !!} @if ($errors->has('bu_rent'))
                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $errors->first('bu_rent') }}</strong>
                                                                    </span>                                                    @endif
                                                    <p class="help-block text-danger"></p>
                                                </div>
                                            </div>
                                              
                                            <div class="col-md-10">
                                                <div class="form-group">
                                                        <label for="">Statue : </label>
                                                    {!! Form::select('bu_status',['0'=>'not enabled',"1"=>"enabled"]) !!} @if ($errors->has('bu_status'))
                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $errors->first('bu_status') }}</strong>
                                                                    </span>                                                    @endif
                                                    <p class="help-block text-danger"></p>
                                                </div>
                                            </div>

                                                  <div class="col-md-10">
                                                <div class="form-group">
                                                        <label for="">Type : </label>
                                                    {!! Form::select('bu_type',['0'=>'apartment',"1"=>"vila"]) !!} @if ($errors->has('bu_type'))
                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $errors->first('bu_type') }}</strong>
                                                                    </span>                                                    @endif
                                                    <p class="help-block text-danger"></p>
                                                </div>
                                            </div>

                                               
                                                    <div class="col-md-10">  
                                                               <div class="form-group">
                                                                    <label for="">Key word : </label>
                                                        {!! Form::text('bu_meta',null,['class'=>'form-control']) !!} @if ($errors->has('bu_meta'))
                                                        <span class="invalid-feedback" role="alert">
                                                                            <strong>{{ $errors->first('bu_meta') }}</strong>
                                                                        </span>                                                    @endif
                                                        <p class="help-block text-danger"></p>
                                                    </div>
                                                </div>

                                                      <div class="col-md-10">  
                                                            <label for="">Square : </label>
                                                               <div class="form-group">
                                                        {!! Form::text('bu_square',null,['class'=>'form-control']) !!} @if ($errors->has('bu_square'))
                                                        <span class="invalid-feedback" role="alert">
                                                                            <strong>{{ $errors->first('bu_square') }}</strong>
                                                                        </span>                                                    @endif
                                                        <p class="help-block text-danger"></p>
                                                    </div>
                                                </div>

                                                                             <div class="col-md-10">  
                                                               <div class="form-group">
                                                                    <label for="">Longitude : </label>
                                                        {!! Form::text('bu_longitude',null,['class'=>'form-control']) !!} @if ($errors->has('bu_longitude'))
                                                        <span class="invalid-feedback" role="alert">
                                                                            <strong>{{ $errors->first('bu_longitude') }}</strong>
                                                                        </span>                                                    @endif
                                                        <p class="help-block text-danger"></p>
                                                    </div>
                                                </div>


                                                                         <div class="col-md-10">  
                                                               <div class="form-group">
                                                                    <label for="">Latitude : </label>
                                                        {!! Form::text('bu_latitude',null,['class'=>'form-control']) !!} @if ($errors->has('bu_latitude'))
                                                        <span class="invalid-feedback" role="alert">
                                                                            <strong>{{ $errors->first('bu_latitude') }}</strong>
                                                                        </span>                                                    @endif
                                                        <p class="help-block text-danger"></p>
                                                    </div>
                                                </div>

                                                                  <div class="col-md-10">  
                                                               <div class="form-group">
                                                                    <label for="">Small descreption : </label>
                                                        {!! Form::textarea('bu_small_dis',null,['class'=>'form-control']) !!} @if ($errors->has('bu_small_dis'))
                                                        <span class="invalid-feedback" role="alert">
                                                                            <strong>{{ $errors->first('bu_small_dis') }}</strong>
                                                                        </span>                                                    @endif
                                                        <p class="help-block text-danger"></p>
                                                    </div>
                                                </div>

                                                                <div class="col-md-10">  
                                                               <div class="form-group">
                                                                    <label for="">Large descreption : </label>
                                                        {!! Form::textarea('bu_large_dis',null,['class'=>'form-control']) !!} @if ($errors->has('bu_large_dis'))
                                                        <span class="invalid-feedback" role="alert">
                                                                            <strong>{{ $errors->first('bu_large_dis') }}</strong>
                                                                        </span>                                                    @endif
                                                        <p class="help-block text-danger"></p>
                                                    </div>
                                                </div>
                                            </div>

                                    </div>
                                </div>
                                <button type="submit" class="btn btn-app btn-lg" style="clear:both;display:block">
                                        <i class="fa fa-save"></i>
                                        Edit
                                    </button>
                                {!!Form::close()!!}

                            </div>

                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
 
@section('footer')
@endsection