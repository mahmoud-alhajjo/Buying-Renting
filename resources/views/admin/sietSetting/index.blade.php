@extends('admin.layout.layout') 
@section('title') /تعديل إعدادات الموقع
@endsection
 
@section('header')
@endsection
 
@section('content')
<section class="content-header">
    <h1>
        Edit SiteSetting
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{url('/adminpanel')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"><a href="{{url('/adminpanel/sitesetting')}}">Edit SiteSetting</a></li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <!-- /.box-header -->
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Edit SiteSetting</h3>
                    <div class="box-body">
                        <div id="login">
                            <div class="container">
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="section-title">
                                            <h2>Edit SiteSetting</h2>
                                        </div>
                                    <form method="POST" role="form" action="{{url('/adminpanel/sitesetting')}}">
                                            @csrf
                                            <div class="row">
                                                    @foreach($SiteSetting as $setting)
                                                <div class="col-xs-12">
                                                       <div class="col-xs-2">
                                                           {{$setting->slug}}
                                                       </div>
                                                       <div class="col-xs-10">
                                                           @if($setting->type == 0)
                                                        {{Form::text($setting->namesetting,$setting->value,['class'=>'form-control'])}}  
                                                        @else
                                                        {{Form::textarea($setting->namesetting,$setting->value,['class'=>'form-control','rows'=>'5'])}}  
                                                        @endif 
                                                          @if ($errors->has($setting->namesetting))
                                                        <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $errors->first('namesetting') }}</strong>
                                                                    </span>                                                        
                                                          @endif    
                                                        </div>                     
                                                </div> 
                                                <div class="clearfix"></div>
                                                <br>  
                                                @endforeach
                                            </div>
                                            
                                    </div>
                                   
                                </div>
                              
                                <button type="submit" class="btn btn-lg btn-primary" style="clear:both;display:block;margin-top: 5%;float: right;margin-right: 18%;">
                                        <i class="fa fa-edit"></i>
                                        Save Sitteng
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
 
@section('footer')
@endsection