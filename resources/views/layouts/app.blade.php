<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Favicons
            ================================================== -->
            <link href="favicon.ico" rel="icon" type="image/x-icon" />
    <!-- Bootstrap -->
    {!! Html::style("website/css/bootstrap.min.css") !!} {!! Html::style("website/fonts/font-awesome/css/font-awesome.css") !!}
    <!-- Stylesheet
            ================================================== -->
    {!! Html::style("website/css/style.css") !!} {!! Html::style("website/css/nivo-lightbox/nivo-lightbox.css") !!} {!! Html::style("website/css/nivo-lightbox/default.css")
    !!} {!! Html::style("cus/select2.min.css") !!} {!! Html::style("cus/sweetalert2.min.css") !!}
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
              <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
              <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->
    <title>
        {{getSetting()}} / @yield('title')
    </title>

    @yield('header')
</head>

<body>
    <div id="app page-top" data-spy="scroll" data-target=".navbar-fixed-top">
        <!-- Navigation
                        ==========================================-->
        <nav id="menu" class="navbar navbar-default navbar-fixed">
            <div class="container">
          
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                        <a class="navbar-brand " href="{{url('/')}}">HOOOT</a>
                    <button  class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"  aria-expanded="false" aria-label="Toggle navigation" > <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                   
                </div>
               
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="nav navbar-nav navbar-right">
                       <li> @if(Auth::user() && Auth::user()->admin == 1 )<a class="dropdown-item dropout" href="{{url('adminpanel')}}" >dashboard</a>@endif</li>
                        <li class="nav-item"><a href="{{url('/ShowAllBuilding')}}" class="page-scroll">All Buildings</a></li>
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false" v-pre>
                               Buying <span class="caret"></span>
                                        </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                @foreach(bu_type() as $keyType =>$type)
                                <a class="dropout" href="{{url('/search?bu_rent=0&bu_type='.$keyType)}}" >{{$type}}</a>                                @endforeach
                            </div>
                        </li>
              
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false" v-pre>
                                Rents <span class="caret"></span>
                                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                @foreach(bu_type() as $keyType =>$type)
                                <a class="dropout" href="{{url('/search?bu_rent=1&bu_type='.$keyType)}}">{{$type}}</a>                                @endforeach
                            </div>
                        </li>
                        <li class="nav-item"><a href="{{url('/user/create/building')}}" class="page-scroll">Add</a></li>
                        <li class="nav-item"><a href="{{url('/contactus')}}" class="page-scroll">Contact</a></li>
                        <!-- Authentication Links -->
                        @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                        @else
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false" v-pre>
                                            {{ Auth::user()->name }} <span class="caret"></span>
                                        </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item dropout" href="{{ route('logout') }}" style="padding-left:15px;display: block;" onclick="event.preventDefault();
                                                             document.getElementById('logout-form').submit();"><i class="fa fa-sign-out" style="margin-right: 10px;"></i>
                                                {{ __('Logout') }}
                                            </a>
                             
                                            <a class="dropdown-item dropout" href="{{url('/user/editSetting')}}" ><i class="fa fa-user" style="margin-right: 10px;"></i>profile</a>
                                            <a class="dropdown-item dropout" href="{{url('/user/create/building')}}" ><i class="fa fa-plus" style="margin-right: 10px;"></i>Add Building </a>
                                            <a class="dropdown-item dropout" href="{{url('/user/buildingShow')}}"><i class="fa fa-building" style="margin-right: 10px;"></i>My active post </a>
                                            <a class="dropdown-item dropout" href="{{url('/user/buildingShow/waiting')}}"><i class="fa fa-building-o" style="margin-right: 10px;"></i>My inactive post </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>

                            </div>
                        </li>
                        @endguest
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
        </nav>
        <!-- Header -->


        <main class="py-4">
            @yield('content')
        </main>
        <!-- Footer Section -->
        <div id="footer">
            <div class="container text-center">
                <p>{{getSetting('footer')}} &copy; {{date('Y')}}  <a href="#" rel="nofollow">Hoot</a></p>
            </div>
        </div>
    </div>
    {!! Html::script("website/js/jquery.1.11.1.js") !!} {!! Html::script("website/js/bootstrap.js") !!} 
 {!! Html::script("website/js/nivo-lightbox.js") !!} {!! Html::script("website/js/jqBootstrapValidation.js") !!} {!!
    Html::script("website/js/contact_me.js") !!} {!! Html::script("website/js/main.js") !!} {!! Html::script("cus/select2.min.js")
    !!} {!! Html::script("cus/sweetalert2.min.js") !!}
    <script type="text/javascript">
        $('.js-example-basic-single ').select2();
    </script>
    @yield('footer')
    @include('admin.layout.flash_massage')
</body>

</html>