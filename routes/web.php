<?php

#admin Routes
Route::group(['middleware' => ['web','admin']],function(){
    
    #data tables ajax must be first
    Route::get("/adminpanel/users/data",['as' => 'adminpanel.users.data' , 'uses' =>"UsersController@getAddEditRemoveColumnData"] );
    Route::get("/adminpanel/bu/data",['as' => 'adminpanel.bu.data' , 'uses' =>"BuController@getAddEditRemoveColumnData"] );
    Route::get("/adminpanel/contactus/data",['as' => 'adminpanel.contactus.data' , 'uses' =>"ContactUsController@getAddEditRemoveColumnData"] );
  
    #admin
    Route::get("/adminpanel","AdminController@index");
    Route::get("/adminpanel/building/year/statistics","AdminController@yearStatistics");
    Route::post("/adminpanel/building/year/statistics","AdminController@shwoThisyear");
    
    #user
    Route::resource("/adminpanel/users","UsersController");
    Route::post("/adminpanel/user/changePassword","UsersController@updatePassword");
    Route::get("/adminpanel/users/{id}/delete","UsersController@destroy");

     #Setting Site
     Route::get('/adminpanel/sitesetting','SiteSettingController@index');
     Route::post('/adminpanel/sitesetting','SiteSettingController@stor');

    #Building
    Route::resource("/adminpanel/bu","BuController", ['except' => [ 'index' , 'show' ]]);
    Route::get("/adminpanel/bu/{id?}","BuController@index");
    Route::get("/adminpanel/bu/{id}/delete","BuController@destroy");
    Route::get("/adminpanel/change/status/{id}/{status}","BuController@changeStatus");

    #Users Opinions
    Route::resource("/adminpanel/contactus","ContactUsController");
    Route::get("/adminpanel/contactus/{id}/delete","ContactUsController@destroy");

});
#########################################################
#########################################################


#User Routes 



Route::get("/",function(){
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/ShowAllBuilding','BuController@showAllEnable');

Route::get('/forBayOrRent/{rent}','BuController@forBayOrRent');

Route::get('/forByType/{type}','BuController@forByType');

Route::get('/search','BuController@search');

Route::get('/singleBuilding/{id}','BuController@singleBuilding');

// Route::get('/ajax/bu/informatio/','BuController@getAjaxInfo');

Route::get('/contactus','ContactUsController@index1');

Route::post('/contactus','ContactUsController@store');

Route::get('/user/create/building','BuController@userAddView');

Route::post('/user/create/building','BuController@userStore');

Route::get('/user/buildingShow','BuController@showUserBuilding')->middleware('auth');

Route::get('/user/buildingShow/waiting','BuController@showUserBuildingWaiting')->middleware('auth');

Route::get('/user/editSetting','UsersController@userEditProfile')->middleware('auth');

Route::patch('/user/editSetting',['as' => 'user.editSetting' , 'uses' => 'UsersController@userUpdateProfile'])->middleware('auth');

Route::post('/user/changePassword/','UsersController@changePassword')->middleware('auth');

Route::get('/edit/user/building/{id}','BuController@userEditBuilding')->middleware('auth');

Route::patch('/update/user/building','BuController@userUpdateBuilding')->middleware('auth');

// Route::patch('/edit/user/building/{id}',['as' => 'edit.user.building.{id}' , 'uses' => 'BuController@userUpdateBuilding'])->middleware('auth');


