<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bu extends Model
{
    
    protected $table='bu';

    protected $fillable =[
        'bu_name', 
        'bu_place',
        'rooms' ,
        'bu_price',
        'image', 
        'bu_rent', 
        'bu_square', 
        'bu_type', 
        'bu_small_dis', 
        'bu_meta', 
        'bu_longitude', 
        'bu_latitude', 
        'bu_large_dis', 
        'bu_status', 
        'user_id',
        'month',
        'year',
    ];
}
