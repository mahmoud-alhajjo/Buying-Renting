<?php

function getSetting($settingname='sitename')
{
    return \App\SiteSetting::where('namesetting',$settingname)->get()[0]->value;
}

function checkIfImageIsexist($imageName)
{
    if($imageName != '')
    {
    $path = base_path().'/public/website/bu_image/'.$imageName;
    if(file_exists($path))
        {
            return Request::root().'/website/bu_image/'.$imageName;
        }
    }else{
        return getSetting('no_image');
    }
}

function checkIfImageIsexistUser($imageName)
{
    if($imageName != '')
    {
    $path = base_path().'/public/website/user_image/'.$imageName;
    if(file_exists($path))
        {
            return Request::root().'/website/user_image/'.$imageName;
        }
    }else{
        return getSetting('no_image_user');
    }
}

function bu_type()
{
    $array= ['apartment','vila'];
    return $array;
}

function bu_rent()
{
    $array= ['sale','rent'];
    return $array;
}

function bu_status()
{
    $array= ['not enabled','enabled'];
    return $array;
}

function rooms()
{
    $array= [];
    for ($i = 1; $i <= 40; $i++) 
    {
        
        $array[$i]=$i;
    }
    return $array;
}

function bu_place()  
{
    $array=
    [
        "1"=>"Damascus",
        "2"=>"Aleppo",   
        "3"=>"Hosm",
        "4"=>"Hama",
        "5"=>"Latakia",
        "6"=>"Tartus",
        "7"=>"Quneitra",
        "8"=>"Idlib",
        "9"=>"Daraa",
        "10"=>"As-Suwayda",
        "11"=>"Raqqa",
        "12"=>"Al-Hasakah",
        "13"=>"Deir ez-Zor"
    ];
    return $array;
}

function searchNameFields()
{
    $array=[
        "bu_price"=>"Price Building",
        "bu_place"=>"Place Building",
        "rooms"=>"Number rooms",
        "bu_type"=>"Type",
        "bu_square"=>"Square",
        "bu_rent"=>"Proprty type",
        "bu_price_from"=>"Price Building from",
        "bu_price_to"=>"Price Building to",
    ];
    return $array;
}

function CountUnReadMessage()
{
    return \App\ContactUs::where('readIt', 0 )->count(); 
}

function UnReadMessage()
{
    return \App\ContactUs::where('readIt', 0 )->get(); 
}

function setActive($array , $class="active" ) 
{  
    if(!empty($array))
    {
        $segArray= [];
        foreach($array as $key => $url)
        {
            if(Request::segment($key+1) == $url);
            {
                $segArray[] = $url;
            }
        }
        if(implode($segArray) == implode(Request::segments()))
        {
                return $class;
        }
    }
}

function buildingActiveCount($user_id, $status)
{
    return \App\Bu::where('bu_status', $status)->where('user_id',$user_id)->count();
}
function MessageCount($contentEmailUser)
{
    return \App\ContactUs::where('contact_email',$contentEmailUser)->count();
}

function allBuildigAppendToStatus($status)
{
    return \App\Bu::where('bu_status', $status)->count();
}

function lastSixBuildingsForUser($IdUser)
{
    return \App\Bu::select('image')->where('user_id', $IdUser)->take('6')->orderBy('id', 'desc')->get();
}
