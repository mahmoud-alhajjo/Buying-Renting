<?php

namespace App\Http\Middleware;

use Closure;

use Illuminate\Auth\Middleware\Authenticate;

use Auth;

class IsUserAdmin
{
   
    public function handle($request, Closure $next)
    {
        if(Auth::user()->admin != 1){
            return redirect('home');
        }
        return $next($request);
    }
}
