<?php

namespace App\Http\Controllers;

use App\Bu;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Redirect;
use App\Http\Requests\BuRequest;
use App\User;
use DataTables;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
class BuController extends Controller
{
    public function index(Request $request )
    {
        $id = $request->id !== null ? "?user_id=".$request->id : null;
        return view('admin.bu.index', compact('id'));
    }

    public function create()
    {
        return view('admin.bu.add');
    }

    public function store(Request $buRequest, BU $bu)
    {
        if($buRequest->file('image'))
        {
            $fileName = $buRequest->file('image')->getClientOriginalName();
            $buRequest->file('image')->move(base_path().'/public/website/bu_image/',$fileName);
            $image=$fileName;
        }else{
            $image='';
        }
        $user = Auth::user();
        $data = [
            'bu_name' => $buRequest->bu_name,
            'bu_place' => $buRequest->bu_place,
            'rooms' => $buRequest->rooms,
            'bu_price' => $buRequest->bu_price,
            'bu_rent' => $buRequest->bu_rent,
            'bu_square' => $buRequest->bu_square,
            'bu_type' => $buRequest->bu_type,
            'bu_small_dis' => $buRequest->bu_small_dis,
            'bu_meta' => $buRequest->bu_meta,
            'bu_longitude' => $buRequest->bu_longitude,
            'bu_latitude' => $buRequest->bu_latitude,
            'bu_large_dis' => $buRequest->bu_large_dis,
            'bu_status' => $buRequest->bu_status,
            'user_id' => $user->id,
            'image' => $image,
            'month' => date('m'),
            'year' => date('Y'),
        ];

        $bu->create($data);
        return redirect('/adminpanel/bu')->withFlashMassage('Done Register');
    }

    public function edit($id, User $user)
    {
        $bu = Bu::find($id);
        if($bu->user_id == 0)
        {
            $user = "Visitor";
        }else{
        $user = $user->where('id', $bu->user_id)->get()[0];
        }
        return view('admin.bu.edit', compact('bu','user'));
    }

    public function update(Request $buRequest, $id)
    {
        $buUpdate = Bu::find($id);
      
        if($buRequest->file('image'))
        {
            //delete old image
            $deleteFileWithOldName= base_path().'/public/website/bu_image/'.$buUpdate->image;
            if($deleteFileWithOldName != '')
            {
                \Illuminate\Support\Facades\File::delete($deleteFileWithOldName);
            }
            //add new image
            $fileName = $buRequest->file('image')->getClientOriginalName();
            $buRequest->file('image')->move(base_path().'/public/website/bu_image/',$fileName);
            $buUpdate->fill(['image'=> $fileName])->save();
        } 
        $buUpdate->fill(array_except($buRequest->all(),['image']))->save();
        return redirect('/adminpanel/bu')->withFlashMassage('Done Edit');
    }

    public function destroy($id, BU $bu)
    {
        $bu->find($id)->delete();
        return \Redirect::back()->withFlashMassage('Done Delete Building');
    }

    public function getAddEditRemoveColumn()
    {
        return view('admin.bu.index');
    }

    public function getAddEditRemoveColumnData(Request $request ,Bu $bu)
    {
        $bus = Bu::select(['id', 'bu_name', 'rooms', 'bu_price','bu_type','bu_status','bu_place', 'created_at']);

        if($request->user_id)
        {
            $bus = $bu->where('user_id',$request->user_id)->get();
        }
        return Datatables::of($bus)
            ->addColumn('action', function ($bu) {
                if($bu->bu_status == 0 ){
                    return '<a href="/adminpanel/bu/'.$bu->id.'/edit" class="btn btn-xs btn-success"><i class="fa fa-edit"></i> edit</a>'.'  '.
                    '<a href="/adminpanel/bu/'.$bu->id.'/delete" class="btn btn-xs btn-danger"><i class="fa fa-remove"></i> delete</a>'.'  '.
                    '<a href="/adminpanel/change/status/'.$bu->id.'/1" class="btn btn-xs btn-warning"><i class="fa fa-check"></i>enable</a>';
                }else
                return '<a href="/adminpanel/bu/'.$bu->id.'/edit" class="btn btn-xs btn-success"><i class="fa fa-edit"></i> edit</a>'.'  '.
                '<a href="/adminpanel/bu/'.$bu->id.'/delete" class="btn btn-xs btn-danger"><i class="fa fa-remove"></i> delete</a>';
            })
            ->editColumn('bu_type', function ($bu) {
                return $bu->bu_type == 0 ? 'appartment' : 'vila';
            })
            ->editColumn('bu_status', function ($bu) {
                return $bu->bu_status == 0 ? 'not enabled' : 'enabled';
            })
            ->editColumn('bu_place', function ($bu) {
                $a="";
                foreach(bu_place() as $key=>$value)
                {
                    if($bu->bu_place == $key )
                    {
                        $a= $value;
                    }
                }
                return $a;
            })
            ->editColumn('id', '{{$id}}')
            ->removeColumn('password')
            ->make(true);
    }

    public function showAllEnable(Bu $bu)
    {
        $buAll = $bu->where('bu_status', 1)->orderBy('id', 'desc')->paginate(12);
        return view('website.buAll', compact('buAll'));
    }

    public function forBayOrRent($rent, Bu $bu)
    {
        if (in_array($rent, ['0', '1'])) {
            $buAll = $bu->where('bu_status', 1)->where('bu_rent', $rent)->orderBy('id', 'desc')->paginate(12);
            return view('website.buAll', compact('buAll'));
        } else {
            return \Redirect::back();
        }
    }

    public function forByType($type, Bu $bu)
    {
        if (in_array($type, ['0', '1'])) {
            $buAll = $bu->where('bu_status', 1)->where('bu_type', $type)->orderBy('id', 'desc')->paginate(12);
            return view('website.buAll', compact('buAll'));
        } else {
            return \Redirect::back();
        }
    }

    public function search(Request $request, Bu $bu)
    {
       

        if($request->bu_price_from  >= $request->bu_price_to && $request->bu_price_to != "" && $request->bu_price_from != ""){
            $error="you can't enter value in input from bigger or equal than input to that illogical ";
            return \Redirect::back()->withFlashMassage($error);
        }        
        //This way dose searchin only in page
        $requestall = array_except($request->toArray(), ['submit', '_token','page']);
        $query = DB::table('bu') ->select('*');
        $array = [];
        $count = count($requestall);
        $i=0;
        foreach($requestall as $key=>$req){
            $i++;
            if($req != "")
            {
                if($key == 'bu_price_from' && $request->bu_price_to == "")
                {
                    $query->where('bu_price',">=", $req);
                }elseif($key == 'bu_price_to' && $request->bu_price_from == "")
                {
                    $query->where('bu_price',"<=", $req);
                }else
                {
                    if($key != 'bu_price_from' && $key != "bu_price_to")
                    {
                        $query->where($key,$req);
                    }
                }
                $array[$key]=$req;
            }elseif($count == $i && $request->bu_price_from != '' && $request->bu_price_to != ''){
                $query->whereBetween('bu_price',[$request->bu_price_from,$request->bu_price_to]);
                $array[$key]=$req;
                
            }
        }
        $buAll = $query->where('bu_status',1)->paginate(12);
        return view('website.buAll', compact('buAll', 'array'));
        
        //anouther way to dose searshing "queryWay search of all database"
        /*$requestall=array_except($request->toArray(),['submit','_token']);

    $out='';
    $i=0;
    foreach($requestall as $key => $req){
    if($req != '')
    {
    $where= $i == 0 ? " where" : " and";
    $out .= $where. " " . $key ."=" .$req;
    $i=2;
    }
    }

    $query="select * from bu" . $out;
    $buAll=DB::select($query);
    $search="search";
    return view('website.buAll',compact('buAll','search'));*/
    }

    public function singleBuilding($id,Bu $bu)
    {
        $buInfo=$bu->findorFail($id);
        if($buInfo->bu_status == 0)
        {
            $messagetitle = "Wait Approve";
            $messagebody = "This building  <strong>$buInfo->bu_name</strong> is expected to be approved by the administration, Knowing that the property is piblished in a period not exceeding <b>5</b> hours.";
            return view('website.noAppear',compact('buInfo','messagetitle','messagebody'));
        }//bu->take('4')->orderBy('id', 'desc')->get()
        $sem_rent = $bu->where( 'bu_rent' , $buInfo->bu_rent )->where( 'bu_type' , $buInfo->bu_type )->where('bu_status',1)->orderBy(DB::raw('RAND()'))->take(3)->get();
        //  $sem_type = $bu->where( 'bu_type' , $buInfo->bu_type )->where('bu_status',1)->orderBy(DB::raw('RAND()'))->take(3)->get();
        return view('website.singlebu', compact('buInfo','sem_rent','sem_type'));
    }

    public function getAjaxInfo(Request $request , Bu $bu)
    {
        return $bu->find($request->id)->toJson();
    }

    public function userAddView()
    {
        return view('website.userbuilding.useradd');
    }
   
    public function userStore(BuRequest $buRequest, BU $bu)
    {
        if($buRequest->file('image'))
        {
            $fileName = $buRequest->file('image')->getClientOriginalName();
            $buRequest->file('image')->move(base_path().'/public/website/bu_image/',$fileName);
            $image=$fileName;
        }else{
            $image='';
        }
        $user = Auth::user() ? Auth::user()->id: 0;
        $data = [
            'bu_name' => $buRequest->bu_name,
            'bu_place' => $buRequest->bu_place,
            'rooms' => $buRequest->rooms,
            'bu_price' => $buRequest->bu_price,
            'bu_rent' => $buRequest->bu_rent,
            'bu_square' => $buRequest->bu_square,
            'bu_type' => $buRequest->bu_type,
            'bu_large_dis' => $buRequest->bu_large_dis,
            'bu_small_dis' => strip_tags(str_limit($buRequest->bu_large_dis,150)),
            'bu_meta' => $buRequest->bu_meta,
            'bu_longitude' => $buRequest->bu_longitude,
            'bu_latitude' => $buRequest->bu_latitude,
            'user_id' => $user,
            'image' => $image,
            'month' => date('m'),
            'year' => date('Y'),
        ];
        $bu->create($data);
        return redirect('/')->withFlashMassage('The building has been successfully added');
    }

    public function showUserBuilding(Bu $bu)
    {   
        $user = Auth::user();
        $buUser = $bu->where('user_id',$user->id)->where('bu_status',1)->paginate(10);
        return view('website.userbuilding.showUserBuilding',compact('buUser','user'));
    }
    
    public function showUserBuildingWaiting(Bu $bu)
    {   
        $user = Auth::user();
        $buUser = $bu->where('user_id',$user->id)->where('bu_status',0)->paginate(10);
        return view('website.userbuilding.showUserBuilding',compact('buUser','user'));
    }

    public function userEditBuilding($id , Bu $bu)
    {
        $user = Auth::user();
        $buInfo = $bu->find($id);

        if($user->id != $buInfo->user_id)
        {
            $messagetitle = "Warning";
            $messagebody = "<b>Warning:</b> This building You didn't add it, Another account did that, Please don't try to be smart here.";
            return view('website.noAppear',compact('buInfo','messagetitle','messagebody'));
        }elseif($buInfo->bu_status == 1){
            $messagetitle = "Alert";
            $messagebody = "<b>Alert:</b> This building <b>$buInfo->bu_name</b> activated, You can't modify it now if you want to modify, You have to send a message, by <strong>Contact Us</strong>.";
            return view('website.noAppear',compact('buInfo','messagetitle','messagebody'));
        }
        $bu = $buInfo;
        return view('website.userbuilding.userEditBuilding',compact('bu','user'));
    }

    
    public function userUpdateBuilding(BuRequest $request , Bu $bu)
    {
        $buUpdate = $bu->find($request->bu_id);//you must pass bu_id
      
        if($request->file('image'))
        {
            //delete old image
            $deleteFileWithOldName= base_path().'/public/website/bu_image/'.$buUpdate->image;
            if($deleteFileWithOldName != '')
            {
                \Illuminate\Support\Facades\File::delete($deleteFileWithOldName);
            }
            //add new image
            $fileName = $request->file('image')->getClientOriginalName();
            $request->file('image')->move(base_path().'/public/website/bu_image/',$fileName);
            $buUpdate->fill(['image'=> $fileName])->save();
        } 
        $buUpdate->fill(array_except($request->all(),['image']))->save();
        $buUpdate->fill(['bu_small_dis' => strip_tags(str_limit($request->bu_large_dis,150))]);
        return \Redirect::back()->withFlashMassage('Done Edit');
    }

    public function changeStatus($id, $status, Bu $bu)
    {
        $buUpdate = $bu->find($id);
        $buUpdate->fill(['bu_status'=> $status])->save();
        if($buUpdate->bu_status == 1)
        {
            return \Redirect::back()->withFlashMassage('Done Activeted');
        }else{
            return \Redirect::back()->withFlashMassage('Done Inactiveted');
        }
    }
}