<?php

namespace App\Http\Controllers;

use App\Bu;

use App\Http\Controllers\Controller;

use App\Http\Controllers\Redirect;

use App\Http\Requests\AddUserRequestAdmin;

use App\Http\Requests\UserUpdateRequest;

use App\Http\Requests\UserChangePasswordRequest;

use App\User;

use DataTables;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    public function index()
    {
        return view('admin.user.index');
    }

    public function create()
    {
        return view('admin.user.add');
    }

    public function store(AddUserRequestAdmin $request, User $user)
    {
        if($request->file('image'))
        {
            $fileName = $request->file('image')->getClientOriginalName();
            $request->file('image')->move(base_path().'/public/website/user_image/',$fileName);
            $image=$fileName;
        }else{
            $image='';
        }
        $user->create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'image' => $image,
        ]);
        return redirect('/adminpanel/users')->withFlashMassage('Done Register');
    }

    public function edit(User $user,Bu $bu)
    {
        $buWaiting = $bu->where('bu_status',0)->where('user_id',$user->id)->get();
        $buenable = $bu->where('bu_status',1)->where('user_id',$user->id)->get();
        return view('admin.user.edit', compact('user', 'buWaiting', 'buenable'));
    }

    public function update(Request $request, $id)
    {
        $userUpdate = User::find($id);
        if($request->file('image'))
        {
            //delete old image
            $deleteFileWithOldName= base_path().'/public/website/user_image/'.$userUpdate->image;
            if($deleteFileWithOldName != '')
            {
                \Illuminate\Support\Facades\File::delete($deleteFileWithOldName);
            }
            //add new image
            $fileName = $request->file('image')->getClientOriginalName();
            $request->file('image')->move(base_path().'/public/website/user_image/',$fileName);
            $userUpdate->fill(['image'=> $fileName])->save();
        } 

         $userUpdate->name = $request->name;
         $userUpdate->email = $request->email;
         $userUpdate->admin = $request->admin;
         $userUpdate->update();
        #$userUpdate->fill($request->all())->save();
        return \Redirect::back()->withFlashMassage('Done Edit');
    }

    public function updatePassword(Request $request)
    {
        $userUpdate = User::find($request->user_id);
        $userUpdate->password = Hash::make($request->password);
        $userUpdate->update();
        #$userUpdate->fill($request->all())->save();
        return \Redirect::back()->withFlashMassage('Done Edit');
    }

    public function destroy($id, User $user)
    {
        $user->find($id)->delete();
        Bu::where('user_id', $id)->delete();
        return redirect('/adminpanel/users')->withFlashMassage('Done Delete Member');
    }

    public function getAddEditRemoveColumn()
    {
        return view('admin.user.index');
    }

    public function getAddEditRemoveColumnData()
    {
        $users = User::select(['id', 'name', 'email', 'created_at','admin']);
        return Datatables::of($users)
            ->addColumn('action', function ($user) {
                if($user->id != 1)
                return '<a href="/adminpanel/users/'.$user->id.'/edit" class="btn btn-xs btn-success"><i class="fa fa-edit"></i> edit</a>'.'  '.
                '<a href="/adminpanel/users/'.$user->id.'/delete" class="btn btn-xs btn-danger"><i class="fa fa-remove"></i> delete</a>'.'  '.
                '<a href="/adminpanel/bu/'.$user->id.'" class="btn btn-xs btn-warning"><i class="fa fa-home"></i>  Buildings</a>';
            })
            ->editColumn('admin', function ($user) {
                return $user->admin == 1 ? 'admin' : 'user';
            })
            ->editColumn('name', function ($user) {
                return \Html::link('/adminpanel/users/' .$user->id .'/edit',$user->name);
            })
            ->editColumn('id', '{{$id}}')
            ->removeColumn('password')
            ->make(true);
    }

    public function userEditProfile()
    {
        $user = Auth::user();
        return view('website.userbuilding.profile',compact('user'));
    }

    public function userUpdateProfile(UserUpdateRequest $request , User $userEmailExist)
    {
        $user = Auth::user();
      
        if($request->file('image'))
        {
            //delete old image
            $deleteFileWithOldName= base_path().'/public/website/user_image/'.$user->image;
            if($deleteFileWithOldName != '')
            {
                \Illuminate\Support\Facades\File::delete($deleteFileWithOldName);
            }
            //add new image
            $fileName = $request->file('image')->getClientOriginalName();
            $request->file('image')->move(base_path().'/public/website/user_image/',$fileName);
            $user->fill(['image'=> $fileName])->save();
        } 

        if($request->email != $user->email)
        {
            $checkEmail = $userEmailExist->where('email' , $request->email)->count();
            if($checkEmail == 0)
            {
                $user->fill($request->all())->update();
            }else{
                return \Redirect::back()->withFlashMassage('This email already exists, Please use another email');
            }
        }else{
            $user->fill(['name' => $request->name])->update();
        }

        return \Redirect::back()->withFlashMassage('Done Edit');

    }
    public function changePassword(UserChangePasswordRequest $request , User $users)
    {
        $user = Auth::user();

        if (Hash::check($request->password, $user->password)) 
        {
            $hash = Hash::make($request->newpassword);
            $user->fill(['password' => $hash])->save();
            return \Redirect::back()->withFlashMassage("Password changed successfully");
        }else{
            return \Redirect::back()->withFlashMassage("The password entered dosen't match our previously registered password");
        }        
    }

}