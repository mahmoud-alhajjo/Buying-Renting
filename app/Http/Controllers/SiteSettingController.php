<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\SiteSetting;

use App\Http\Controllers\Controller;

class SiteSettingController extends Controller
{
    public function index(SiteSetting $SiteSetting)
    {
        $SiteSetting = $SiteSetting->all();
        return view('admin.sietSetting.index',compact('SiteSetting'));
    }

    public function stor(Request $request,SiteSetting $SiteSetting)
    {      
       foreach(array_except($request->toArray(),['_token' ,'submit']) as $key => $req )
       {
          $SiteSettingUpdate = $SiteSetting->where('namesetting' , $key)->get()[0];
          //هون عم اعمل سليكت على قاعدة البيانات جبلي من الجدول سايت ستنغ لما يكون الحقل يلي اسمو نيم سيتنغ بيساوي ال كي يلي هو اسم الحقل يلي متسجل فيه
          $SiteSettingUpdate->fill(['value'=>$req])->save();
       }
       return  \Redirect::back()->withFlashMassage('Done Edit Setthng');
    }
}
