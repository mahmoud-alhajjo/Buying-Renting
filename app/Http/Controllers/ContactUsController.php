<?php

namespace App\Http\Controllers;

use App\ContactUs;

use App\Http\Controllers\Controller;

use App\Http\Controllers\Redirect;

use App\Http\Requests\ContactUsRequest;

use DataTables;

use DB;

use Illuminate\Http\Request;

class ContactUsController extends Controller 
{
    public function index(ContactUs $contact)
    {
        $contact = $contact->all();
        return view('admin.contactus.index', compact('contact'));
    }

    public function index1()
    {
        return view('website.userbuilding.contactus');
    }

    public function store(ContactUsRequest $request, ContactUs $contact)
    {
        $contact->create($request->all());
        return redirect('/')->withFlashMassage('Your Message sent');
    }

    public function edit($id)
    {
        $contact = ContactUs::find($id);
        $contact->fill(['readIt' => 1])->save();
        return view('admin.contactus.edit', compact('contact'));
    }
    
    public function update(ContactUsRequest $request, $id)
    {
        $contactUpdate = ContactUs::find($id);
        $contactUpdate->contact_name = $request->contact_name;
        $contactUpdate->contact_email = $request->contact_email;
        $contactUpdate->contact_message = $request->contact_message;
        $contactUpdate->update();
        return \Redirect::back()->withFlashMassage('Done Edit');
    }

    public function destroy($id, ContactUs $contact)
    {
        $contact->find($id)->delete();
        return redirect('/adminpanel/contactus')->withFlashMassage('Done Delete User Opinion');
    }

    public function getAddEditRemoveColumn()
    {
        return view('admin.contactus.index');
    }

    public function getAddEditRemoveColumnData()
    {
        $contacts = ContactUs::select(['id', 'contact_name', 'contact_email', 'contact_message', 'readIt' ,'created_at']);
        return Datatables::of($contacts)
            ->editColumn('readIt', function ($contact) {	
                return $contact->readIt == 1 ? 'Done Read it' : 'Not Read it yet';
            })
            ->addColumn('action', function ($contact) {
                if($contact->readIt == 0){
                return '<a href="/adminpanel/contactus/'.$contact->id.'/edit" class="btn btn-xs btn-success"><i class="fa fa-check"></i>  Read the message</a>'."  ".'<a href="/adminpanel/contactus/'.$contact->id.'/delete" class="btn btn-xs btn-danger"><i class="fa fa-remove"></i> delete</a>';   
                }
                return   '<a href="/adminpanel/contactus/'.$contact->id.'/delete" class="btn btn-xs btn-danger"><i class="fa fa-remove"></i> delete</a>';
            })
            ->editColumn('contact_name', function ($contact) {
                return \Html::link('/adminpanel/contactus/' .$contact->id .'/edit',$contact->contact_name);
            })
            ->editColumn('id', '{{$id}}')
            ->removeColumn('password')
            ->make(true);
    }
}
