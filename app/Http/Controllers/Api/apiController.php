<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Http\Resources\BuResources;

use App\Bu;

class apiController extends Controller
{

  use apiResponseTrait;

  public function index()
  {
    $data = BuResources::collection(Bu::get()); 
    return $this->apiResponse($data);
  }

  public function show($id)
  {
    $building = new BuResources(Bu::find($id)); 
    if($building)
    {
      return  $this->apiResponse($building);
    }else{
      return $this->apiResponse(null, "This Building not exist", 404);
    }
  }

}
