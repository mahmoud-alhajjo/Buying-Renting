<?php

namespace App\Http\Controllers\Api;

trait apiResponseTrait
{
    /*
    [
    'data' => 
    'status' => 'true/false'
    'error' => ''
    ]
    */ 

    public function apiResponse($data = null, $error = null, $code = 200)
    {
        $successCode=[200, 201, 202];
        $array=
        [
            'data' => $data,
            'status' => in_array($code, $successCode) ? true : false,
            'error' => $error,
        ];

        return response($content = $array, $code);
    }
    
}

