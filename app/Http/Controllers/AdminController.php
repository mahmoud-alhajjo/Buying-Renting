<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

use App\Bu;

use DB;

use App\ContactUs;

use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function index(User $user, Bu $bu, ContactUs $contact)
    {
        $buildingActive = allBuildigAppendToStatus(1);
        $buildingInactive = allBuildigAppendToStatus(0);
        $userCount = $user->count();
        $contactusCount = $contact->count();
        $mapping = $bu->select('bu_latitude','bu_longitude','bu_name')->get();
        $charts = $bu->select(DB::raw('COUNT(*) as counting, month'))->where('year',date('Y'))->groupBy('month')->orderBy('month', 'asc')->get()->toArray();
        $array=[];
        if(isset($charts[0]['month']))
        {
            for($i =1;$i < $charts[0]['month'];$i++ )
            {
               $array[]= 0;
            }
        }
        $new = array_merge($array, $charts);
        $latestUsers = $user->take('8')->orderBy('id', 'desc')->get();
        $latestBuildings = $bu->take('4')->orderBy('id', 'desc')->get();
        $latestContact = $contact->take('6')->orderBy('id', 'desc')->get();
        return view('admin.home.index',compact('buildingActive',
                                               'buildingInactive',
                                                'userCount',
                                                'contactusCount',
                                                'mapping',
                                                'new',
                                                'latestUsers',
                                                'latestBuildings',
                                                'latestContact'));
    }

    public function yearStatistics(Bu $bu)
    {
        $year = date('Y');
        $charts = $bu->select(DB::raw('COUNT(*) as counting, month'))->where('year', $year)->groupBy('month')->orderBy('month', 'asc')->get()->toArray();
        $array=[];
        if(isset($charts[0]['month']))
        {
            for($i =1;$i < $charts[0]['month'];$i++ )
            {
               $array[]= 0;
            }
        }
        $new = array_merge($array, $charts);
      
        return view('admin.home.statistic',compact('year', 'new'));
    }

    public function shwoThisyear(Bu $bu, Request $request)
    {
        $year = $request->year;
        $charts = $bu->select(DB::raw('COUNT(*) as counting, month'))->where('year', $year)->groupBy('month')->orderBy('month', 'asc')->get()->toArray();
        
        $array=[];
        if(isset($charts[0]['month']))
        {
            for($i =1;$i < $charts[0]['month'];$i++ )
            {
               $array[]= 0;
            }
        }
        $new = array_merge($array, $charts);
        return view('admin.home.statistic',compact('year', 'new'));
    }
}
