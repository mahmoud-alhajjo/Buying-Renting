<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BuResources extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'ID' => $this->id,
            'Building name' => $this->bu_name , 
            'place' => $this->bu_price ,
            'rooms' => $this->rooms ,
            'price'  => $this->bu_price , 
            'ownership'  =>  $this->bu_rent ==  0 ? 'sale' : 'rent' , 
            'square'  => $this->bu_square , 
            'type'  => $this->bu_type == 0 ? 'appartment' : 'vila' , 
            'description for google'  => str_limit($this->bu_small_dis , 20) , 
            'status' => $this->bu_status == 1 ? 'active' : 'inactive' , 
            'user_id'  => $this->user_id ,
            'month'  => $this->month ,
            'year'  => $this->year ,
        ];
    }
}
