<?php

namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class BuRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->sanitize();
        return [
            'bu_name' => 'required|string|min:5|max:100',
            'rooms' =>'required|integer',
            'bu_price' => 'required|numeric',
            'bu_rent' =>'required|integer',
            'bu_place' => 'required',
            'bu_large_dis'=>'required|string|min:5|max:1000',
            'bu_type'=> 'required|integer',
            'image'   =>'nullable|mimes:jpg,png,jpeg' ,
            'bu_square' =>'integer|min:2|max:5001',
            'bu_small_dis' => 'nullabl|min:5|max:160',
            'bu_meta' => 'nullable|string|min:2|max:30',
            'bu_latitude' =>'nullable|numeric|min:2|',
            'bu_longitude' =>'nullable|numeric|min:2',     
            'bu_status' =>'integer',           
        ];
    }

    public function sanitize()
    {
        $input = $this->all();

        $input['bu_name'] = filter_var($input['bu_name'], FILTER_SANITIZE_STRING);
        $input['bu_price'] = filter_var($input['bu_price'], FILTER_SANITIZE_NUMBER_FLOAT);
        $input['bu_square'] = filter_var($input['bu_square'], FILTER_SANITIZE_NUMBER_INT);
        $input['bu_large_dis'] = filter_var($input['bu_large_dis'], FILTER_SANITIZE_STRING);

        $this->replace($input);     
    }
}
